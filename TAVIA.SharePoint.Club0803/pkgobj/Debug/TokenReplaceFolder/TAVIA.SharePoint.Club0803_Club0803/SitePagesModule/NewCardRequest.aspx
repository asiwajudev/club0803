﻿<%@ Assembly Name="TAVIA.SharePoint.Club0803, Version=1.0.0.0, Culture=neutral, PublicKeyToken=e94667b04745ceb1" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewCardRequest.aspx.cs" Inherits="TAVIA.SharePoint.Club0803.SitePagesModule.NewCardRequest" MasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <style>
        #sideNavBox {
            display: none;
        }

        #contentBox {
            margin-right: 20px;
            margin-left: 20px;
        }

        .h4, h4 {
            font-size: 1.5rem;
        }
        #s4-ribbonrow{
			display:none !important;
		}
		#s4-titlerow{
			display:none !important;
		}
		.ms-core-pageTitle{
			display:none !important;
		}

    </style>

    <SharePoint:UIVersionedContent ID="UIVersionedContent1" UIVersion="4" runat="server">
        <contenttemplate>
		<SharePoint:CssRegistration Name="forms.css" runat="server"/>
	</contenttemplate>
    </SharePoint:UIVersionedContent>

    <link href="/_layouts/15/club0803/styles/bootstrap.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-grid.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-reboot.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/style.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
<section class="col-md-12 form-container pb-3">
        <h2 class="title-container text-center py-3">Request a new Card</h2>
        <div class="form-bg">
            <div class="row p-3">
                <div class="col-md-12">
                    <h5 class="text-blue">Card Details</h5>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Vendor<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:DropDownList ID="ddlVendor" class="form-control mtn-input" runat="server" >
                            </asp:DropDownList>
                            <SharePoint:InputFormRequiredFieldValidator ID="rfvVendor"
                                class="ms-error" ControlToValidate="ddlVendor" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Phone Number<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                             <SharePoint:InputFormTextBox ID="txtPhoneNumber" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="13" title="Vendor Phone Number" class="form-control mtn-input mtn-textbox" />
                              <asp:RegularExpressionValidator ID="rgxvPhoneNumber" runat="server" ControlToValidate="txtPhoneNumber"
                                ErrorMessage="Please Enter a Valid Phone Number" class="ms-error" Display="Dynamic"
                                ValidationExpression="^\d{11,13}$" SetFocusOnError="true"></asp:RegularExpressionValidator>
                               <SharePoint:InputFormRequiredFieldValidator ID="rfvPhoneNumber"
                                class="ms-error" ControlToValidate="txtPhoneNumber" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Date Required<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                             <SharePoint:DateTimeControl ID="dtcDateRequired" LocaleId="2057" ToolTip="DD/MM/YYYY"
                                runat="server" DateOnly="true" CssClassTextBox="form-control mtn-input mtn-textbox"></SharePoint:DateTimeControl>
                            <asp:RequiredFieldValidator ID="rfvDateRequired" runat="server" ControlToValidate="dtcDateRequired$dtcDateRequiredDate"
                                class="ms-error" Display="Dynamic" ErrorMessage="You must specify a value for this required field."></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Job Location<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:DropDownList ID="ddlLocation" class="form-control mtn-input" runat="server" >
                            </asp:DropDownList>
                            <SharePoint:InputFormRequiredFieldValidator ID="rfvLocation"
                                class="ms-error" ControlToValidate="ddlLocation" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Comments</label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                              <asp:TextBox ID="txtComments" runat="server" Rows="3" TextMode="MultiLine" class="form-control mtn-input mtn-textbox"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <hr class="mtn-divider m-0 px-3" />
            </div>

            <!-- button section -->
            <div class="col-md-12 py-3">
                <div class="row btn-div text-center">
                    <asp:Button ID="btnSubmit" class="btn mtn-btn mr-3" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                    <asp:Button ID="btnCancel" class="btn mtn-btn mr-3" CausesValidation="false" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                </div>
            </div>
        </div>
    </section>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Club0803 - Request A new Card
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" />

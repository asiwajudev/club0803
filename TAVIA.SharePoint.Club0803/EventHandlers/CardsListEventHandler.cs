﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAVIA.SharePoint.Club0803.Utility;
using TAVIA.SharePoint.Utilities;

namespace TAVIA.SharePoint.Club0803.EventHandlers
{
    public class CardsListEventHandler : SPItemEventReceiver
    {
        public override void ItemAdded(SPItemEventProperties properties)
        {
            base.ItemAdded(properties);

            SPListItem item = properties.ListItem;
            SendCardExpirationAndAdditionNotificationToAdminAndMembers(item, properties);

            //item["Title"] = String.Format("{0}{1:000000}", "ORD", item.ID);
            //item.SystemUpdate(false);
        }

        private void SendCardExpirationAndAdditionNotificationToAdminAndMembers(SPListItem cardItem, SPItemEventProperties properties)
        {
            string senderEmailSignature = SPUtility.GetLocalizedString("$Resources:SenderEmailSignature", "Club0803_Config", 1033);
            string senderEmailAddress = SPUtility.GetLocalizedString("$Resources:SenderEmailAddress", "Club0803_Config", 1033);
            string superAdminsGroupEmailAddress = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupEmailAddress", "Club0803_Config", 1033);
            string adminsGroupEmailAddress = SPUtility.GetLocalizedString("$Resources:AdminsGroupEmailAddress", "Club0803_Config", 1033);
            // string permanentStaffGroupEmailAddress = SPUtility.GetLocalizedString("$Resources:PermanentStaffGroupEmailAddress", "Club0803_Config", 1033);

            SPFieldUserValueCollection cardHolderUserValueCollection = new SPFieldUserValueCollection(properties.Web, cardItem["CardHolder"].ToString());
            SPUser cardHolderUser = cardHolderUserValueCollection[0].User;
            string cardOwner = string.Empty;
            string cardOwnerEmailAddress = string.Empty;
            string cardOwnerPhoneNumber = string.Empty;
            if (cardHolderUser != null)
            {
                cardOwner = cardHolderUser.Name;
                if (!String.IsNullOrEmpty(cardHolderUser.Email))
                    cardOwnerEmailAddress = cardHolderUser.Email;
            }

            if (cardHolderUser == null)
            {
                cardOwner = cardItem["Card Holder Upload"].ToString();
                cardOwnerEmailAddress = cardItem["Email"].ToString();
            }

            DateTime ExpirationDate = ((DateTime)cardItem["ExpiryDate"]);
            string cardNumber = cardItem["Title"] != null ? cardItem["Title"].ToString() : string.Empty;

            ListDictionary lstReplacements = new ListDictionary();
            lstReplacements.Add("<%" + EmailReplacementKeys.ExpirationDate.ToString() + "%>", ExpirationDate.ToString("dd MMM yyyy"));
            lstReplacements.Add("<%" + EmailReplacementKeys.CardNumber.ToString() + "%>", cardNumber);
            lstReplacements.Add("<%" + EmailReplacementKeys.CardOwner.ToString() + "%>", cardOwner);
            lstReplacements.Add("<%" + EmailReplacementKeys.CardOwnerEmailAddress.ToString() + "%>", cardOwnerEmailAddress);

            lstReplacements.Add("<%" + EmailReplacementKeys.SenderEmailSignature.ToString() + "%>", senderEmailSignature);

            string subjectToAdmin = SPUtility.GetLocalizedString("$Resources:CardExpirationNotificationToAdminEmailSubject", "Club0803_Config", 1033);
            string bodyToAdmin = SPUtility.GetLocalizedString("$Resources:CardExpirationNotificationToAdminEmailBody", "Club0803_Config", 1033);

            string subjectToMembers = SPUtility.GetLocalizedString("$Resources:CardExpirationNotificationToMembersEmailSubject", "Club0803_Config", 1033);
            string bodyToMembers = SPUtility.GetLocalizedString("$Resources:CardExpirationNotificationToMembersEmailBody", "Club0803_Config", 1033);

            EMailHandler.AddReminder(subjectToAdmin, bodyToAdmin, ExpirationDate.AddDays(-7), cardItem.UniqueId, adminsGroupEmailAddress, ccList: superAdminsGroupEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);
            EMailHandler.AddReminder(subjectToMembers, bodyToMembers, ExpirationDate.AddDays(-7), cardItem.UniqueId, cardOwnerEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);

            string cardAddedSubjectToMembers = SPUtility.GetLocalizedString("$Resources:CardAddedNotificationToMembersEmailSubject", "Club0803_Config", 1033);
            string cardAddedBodyToMembers = SPUtility.GetLocalizedString("$Resources:CardAddedNotificationToMembersEmailBody", "Club0803_Config", 1033);

            EMailHandler.SendEmail(cardAddedSubjectToMembers, cardAddedBodyToMembers, cardOwnerEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);

            //CardExpirationNotificationToMembersSMS

            string smsBodyToMembers = SPUtility.GetLocalizedString("$Resources:CardExpirationNotificationToMembersSMS", "Club0803_Config", 1033);

            SMSHandler.AddReminder(smsBodyToMembers, ExpirationDate.AddDays(-7), cardItem.UniqueId, "00000000000", replacements: lstReplacements);

            
        }
    }
}

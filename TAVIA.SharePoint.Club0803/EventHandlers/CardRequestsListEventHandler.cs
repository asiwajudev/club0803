﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAVIA.SharePoint.Club0803.EventHandlers
{
    public class CardRequestsListEventHandler : SPItemEventReceiver
    {
        public override void ItemAdded(SPItemEventProperties properties)
        {
            base.ItemAdded(properties);

            SPListItem item = properties.ListItem;
            item["Title"] = String.Format("{0}{1:000000}", "CR", item.ID);
            item.SystemUpdate(false);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using OfficeOpenXml;
using TAVIA.SharePoint.Utilities;
using System.Collections.Specialized;
using TAVIA.SharePoint.Club0803.Utility;

namespace TAVIA.SharePoint.Club0803.Layouts
{
    public partial class MultipleCardUpload : LayoutsPageBase
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            //dtcClosingDate.MinDate = DateTime.Now.AddDays(-1);
            //dtcClosingDate.MaxDate = DateTime.Now.AddYears(5);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                FillVendors();
                //FillVendorDetails(int.Parse(ddlVendor.Text));
            }
        }

        private void FillVendors()
        {
            string VendorsListInternalName = SPUtility.GetLocalizedString("$Resources:VendorsListUrl", "Club0803_Config", 1033);
            string listUrl = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, VendorsListInternalName);
            SPList VendorsList = SPContext.Current.Web.GetList(listUrl);
            ddlVendor.DataSource = VendorsList.Items.GetDataTable();
            ddlVendor.DataValueField = "ID";
            ddlVendor.DataTextField = "Title";
            ddlVendor.DataBind();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            GoBack();
        }

        private void GoBack()
        {
            if (Page.Request.QueryString["IsDlg"] != null)
            {
                HttpContext context = HttpContext.Current;
                context.Response.Write("<script type='text/javascript'>window.frameElement.cancelPopUp()</script>");
                context.Response.Flush();
                context.Response.End();
            }
            else if (!String.IsNullOrEmpty(Page.Request.QueryString["Source"]))
            {
                Response.Redirect(Page.Request.QueryString["Source"].ToString());
            }
            else
            {
                string CardListInternalName = SPUtility.GetLocalizedString("$Resources:CardsListUrl", "Club0803_Config", 1033);
                string url = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, CardListInternalName);
                Response.Redirect(url);
                //Response.Redirect(SPContext.Current.Web.Url);
            }
        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (fuMultipleCandidates.HasFile)
                {
                    try
                    {
                        byte[] fileData = fuMultipleCandidates.FileBytes;
                        using (MemoryStream memStream = new MemoryStream(fileData))
                        {
                            memStream.Flush();
                            using (ExcelPackage pck = new ExcelPackage(memStream))
                            {
                                if (pck != null)
                                {
                                    ExcelWorksheet ws = pck.Workbook.Worksheets[1];
                                    int rowCount = ws.Dimension.End.Row + 1;
                                    int colCount = ws.Dimension.End.Column + 1;
                                    int startRow = 4;

                                    List<MultipleCardUploads> lstCardItems = new List<MultipleCardUploads>();

                                    //string webURL = SPContext.Current.Web.Url;
                                    //SPSecurity.RunWithElevatedPrivileges(delegate ()
                                    //{
                                    //    using (SPSite impersonatedSite = new SPSite(webURL))
                                    //    {
                                    //        using (SPWeb impersonatedWeb = impersonatedSite.OpenWeb())
                                    //        {
                                    //            impersonatedWeb.AllowUnsafeUpdates = true;

                                    //            string candidatesGroupName = SPUtility.GetLocalizedString("$Resources:Security_Group_Candidates", "ETest_Config", 1033);
                                    //            SPGroup candidatesGroup = impersonatedWeb.SiteGroups[candidatesGroupName];
                                    for (int i = startRow; i < rowCount; i++)
                                    {
                                        bool isRecordValid = true;
                                        MultipleCardUploads item = new MultipleCardUploads();
                                        if (ws.Cells[i, 1].Value != null)
                                        {
                                            //validate
                                            item.CardNumber = ws.Cells[i, 1].Value.ToString();
                                        }
                                        else
                                            isRecordValid = false;
                                        if (ws.Cells[i, 2].Value != null)
                                        {
                                            //validate
                                            item.CardHolder = ws.Cells[i, 2].Value.ToString();
                                        }
                                        else
                                            isRecordValid = false;
                                        if (ws.Cells[i, 3].Value != null)
                                        {
                                            //validate
                                            item.Email = ws.Cells[i, 3].Value.ToString();
                                        }
                                        else
                                            isRecordValid = false;
                                        //if (ws.Cells[i, 4].Value != null)
                                        //{
                                        //    //validate
                                        //    item.PhoneNumber = ws.Cells[i, 4].Value.ToString();
                                        //}
                                        //else
                                        //    isRecordValid = false;
                                        //if (ws.Cells[i, 3].Value != null)
                                        //{
                                        //    //validate
                                        //    item.Vendor = ws.Cells[i, 3].Value.ToString();
                                        //}
                                        //else
                                        //{
                                        //    item.Vendor = "";
                                        //}
                                        if (ws.Cells[i, 4].Value != null)
                                        {
                                            DateTime issueDate;
                                            if (DateTime.TryParseExact(ws.Cells[i, 4].Value.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out issueDate))
                                            {
                                                item.IssueDate = issueDate.ToString("dd/MM/yyyy");
                                            }
                                            else
                                                isRecordValid = false;
                                        }
                                        else
                                            isRecordValid = false;

                                        if (ws.Cells[i, 5].Value != null)
                                        {
                                            DateTime expiryDate;
                                            if (DateTime.TryParseExact(ws.Cells[i, 5].Value.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out expiryDate))
                                            {
                                                item.ExpiryDate = expiryDate.ToString("dd/MM/yyyy");
                                            }
                                            else
                                                isRecordValid = false;
                                        }
                                        else
                                            isRecordValid = false;

                                        if (ws.Cells[i, 6].Value != null)
                                        {
                                            //validate
                                            item.CardType = ws.Cells[i, 6].Value.ToString();
                                        }
                                        else
                                            isRecordValid = false;

                                        //if (ws.Cells[i, 5].Value != null)
                                        //{
                                        //    string paymentOptions = ws.Cells[i, 5].Value.ToString().Trim();
                                        //    if (paymentOptions == "Installment" || paymentOptions == "Full Payment" || paymentOptions == "Not Applicable")
                                        //    {
                                        //        item.PaymentOptions = paymentOptions;
                                        //    }
                                        //    else
                                        //        isRecordValid = false;
                                        //}
                                        //else
                                        //    isRecordValid = false;
                                        //if (ws.Cells[i, 6].Value != null)
                                        //{
                                        //    int interestRate;
                                        //    if (int.TryParse(ws.Cells[i, 6].Value.ToString(), out interestRate))
                                        //    {
                                        //        item.InterestRate = interestRate;
                                        //    }
                                        //    else
                                        //        isRecordValid = false;
                                        //}
                                        //else
                                        //    isRecordValid = false;
                                        //if (ws.Cells[i, 7].Value != null)
                                        //{
                                        //    int discountRate;
                                        //    if (int.TryParse(ws.Cells[i, 7].Value.ToString(), out discountRate))
                                        //    {
                                        //        item.DiscountRate = discountRate;
                                        //    }
                                        //    else
                                        //        isRecordValid = false;
                                        //}
                                        //else
                                        //    isRecordValid = false;
                                        //if (ws.Cells[i, 8].Value != null)
                                        //{
                                        //    //validate
                                        //    item.OtherImportantInformation = ws.Cells[i, 8].Value.ToString();
                                        //}
                                        //else
                                        //    isRecordValid = false;
                                        //if (ws.Cells[i, 9].Value != null)
                                        //{
                                        //    double productPrice;
                                        //    if (double.TryParse(ws.Cells[i, 9].Value.ToString(), out productPrice))
                                        //    {
                                        //        item.ProductPrice = productPrice.ToString();
                                        //    }
                                        //    else
                                        //        item.ProductPrice = "";
                                        //}
                                        //else
                                        //    item.ProductPrice = "";

                                        //if (isRecordValid)
                                        //{
                                        //    item = CreateCandidate(item, candidatesGroup);
                                        //}
                                        //else
                                        //{
                                        //    item.Status = "Invalid";
                                        //}
                                        //public string Status { get; set; }
                                        //DateTime.ParseExact(ws.Cells[i, 7].Value.ToString(),"dd/MM/yyyy",System.Globalization.CultureInfo.InvariantCulture)
                                        if (isRecordValid)
                                            lstCardItems.Add(item);
                                    }
                                    //if (isExcelSheetValid)
                                    //{
                                    repMultipleCardUpload.DataSource = lstCardItems;
                                    repMultipleCardUpload.DataBind();
                                    btnSubmit.Visible = false;
                                    //ddlCategory.Enabled = false;
                                    //ddlType.Enabled = false;
                                    //ddlVendor.Enabled = false;
                                    divUpload1.Visible = false;
                                    divUpload2.Visible = false;
                                    btnSaveItems.Visible = true;

                                    //}
                                    //else
                                    //{
                                    //    //Show error
                                    //}
                                    //           // divControls.Visible = false;

                                    //            impersonatedWeb.AllowUnsafeUpdates = false;
                                    //        }
                                    //    }
                                    //});
                                }
                            }
                        }
                    }
                    catch (Exception Ex1)
                    {
                        // ltrlMsg.Text = "Error Occured <br/>" + Ex1.Message;
                    }
                }
            }

        }

        //private void SendMultipleCardUploadNotification(SPListItem cardItem)
        //{
        //    string senderEmailSignature = SPUtility.GetLocalizedString("$Resources:SenderEmailSignature", "Club0803_Config", 1033);
        //    string senderEmailAddress = SPUtility.GetLocalizedString("$Resources:SenderEmailAddress", "Club0803_Config", 1033);
        //    string superAdminsGroupEmailAddress = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupEmailAddress", "Club0803_Config", 1033);
        //    string adminsGroupEmailAddress = SPUtility.GetLocalizedString("$Resources:AdminsGroupEmailAddress", "Club0803_Config", 1033);

        //    ListDictionary lstReplacements = new ListDictionary();
        //    lstReplacements.Add("<%" + EmailReplacementKeys.DiscountedItemNumber.ToString() + "%>", cardItem.ID.ToString());
        //    string discountedItemUrl = cardItem.ParentList.ParentWeb.Site.MakeFullUrl(cardItem.ParentList.DefaultDisplayFormUrl) + "?ID=" + cardItem.ID.ToString();
        //    lstReplacements.Add("<%" + EmailReplacementKeys.DiscountedItemUrl.ToString() + "%>", "<a href='" + discountedItemUrl + "'>click here</a>");
        //    lstReplacements.Add("<%" + EmailReplacementKeys.SenderEmailSignature.ToString() + "%>", senderEmailSignature);

        //    string subjectToAdmin = SPUtility.GetLocalizedString("$Resources:DiscountedItemUploadNotificationToAdminEmailSubject", "Club0803_Config", 1033);
        //    string bodyToAdmin = SPUtility.GetLocalizedString("$Resources:DiscountedItemUploadNotificationToAdminEmailBody", "Club0803_Config", 1033);

        //    string subjectToSuperAdmin = SPUtility.GetLocalizedString("$Resources:DiscountedItemUploadNotificationToSuperAdminEmailSubject", "Club0803_Config", 1033);
        //    string bodyToSuperAdmin = SPUtility.GetLocalizedString("$Resources:DiscountedItemUploadNotificationToSuperAdminEmailBody", "Club0803_Config", 1033);

        //    EMailHandler.SendEmail(subjectToAdmin, bodyToAdmin, adminsGroupEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);
        //    EMailHandler.SendEmail(subjectToSuperAdmin, bodyToSuperAdmin, superAdminsGroupEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);

        //}

        protected void btnSaveItems_Click(object sender, EventArgs e)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {

                string CardListInternalName = SPUtility.GetLocalizedString("$Resources:CardsListUrl", "Club0803_Config", 1033);
                string CardListListUrl = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, CardListInternalName);
                SPList CardItemList = SPContext.Current.Web.GetList(CardListListUrl);

                foreach (RepeaterItem repItem in repMultipleCardUpload.Items)
                {
                    if (repItem.ItemType == ListItemType.AlternatingItem || repItem.ItemType == ListItemType.Item)
                    {
                        Label lblCardNumber = ((Label)repItem.FindControl("lblCardNumber"));
                        Label lblCardHolder = ((Label)repItem.FindControl("lblCardHolder"));
                        Label lblEmail = ((Label)repItem.FindControl("lblEmail"));
                        Label lblIssueDate = ((Label)repItem.FindControl("lblIssueDate"));
                        Label lblExpiryDate = ((Label)repItem.FindControl("lblExpiryDate"));
                        Label lblCardType = ((Label)repItem.FindControl("lblCardType"));
                        //Label lblPhoneNumber = ((Label)repItem.FindControl("lblPhoneNumber"));
                        //Label lblDiscountRate = ((Label)repItem.FindControl("lblDiscountRate"));
                        //Label lblOtherImportantInformation = ((Label)repItem.FindControl("lblOtherImportantInformation"));
                        //Label lblProductPrice = ((Label)repItem.FindControl("lblProductPrice"));


                        SPListItem item = CardItemList.AddItem();

                        item["Title"] = SPHandler.RemoveHTMLTags(lblCardNumber.Text);

                        item["Card Holder Upload"] = SPHandler.RemoveHTMLTags(lblCardHolder.Text);

                        item["Email Address"] = SPHandler.RemoveHTMLTags(lblEmail.Text);

                        item["CardType"] = SPHandler.RemoveHTMLTags(lblCardType.Text);

                        //item["Phone Number"] = SPHandler.RemoveHTMLTags(lblPhoneNumber.Text);

                        item["Vendor"] = new SPFieldLookupValue(int.Parse(ddlVendor.Text), ddlVendor.SelectedItem.Text); ;

                        //item["Title"] = SPHandler.RemoveHTMLTags(lblCardNumber.Text);
                        //item["Card Holder Upload"] = SPHandler.RemoveHTMLTags(lblCardHolder.Text);
                        //item["Email Address"] = SPHandler.RemoveHTMLTags(lblEmail.Text);                    
                        //item["CardType"] = SPHandler.RemoveHTMLTags(lblCardType.Text);
                        //item["Vendor"] = new SPFieldLookupValue(int.Parse(ddlVendor.Text), ddlVendor.SelectedItem.Text);

                        //if (!String.IsNullOrEmpty(lblCardNumber.Text))
                        //    item["Card Number"] = SPHandler.RemoveHTMLTags(lblVendorP.Text);
                        //else
                        //    item["VendorProductWebsiteURL"] = null;

                        DateTime issueDate;
                        if (DateTime.TryParseExact(lblIssueDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out issueDate))
                        {
                            item["IssueDate"] = issueDate;
                        }

                        DateTime expiryDate;
                        if (DateTime.TryParseExact(lblExpiryDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out expiryDate))
                        {
                            item["ExpiryDate"] = expiryDate;
                        }


                        //DateTime issueDate;
                        //if (DateTime.TryParseExact(lblIssueDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out issueDate))
                        //{
                        //    item["IssueDate"] = issueDate;
                        //}
                        //DateTime expiryDate;
                        //if (DateTime.TryParseExact(lblExpiryDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out expiryDate))
                        //{
                        //    item["ExpiryDate"] = expiryDate;
                        //}


                        //item["PaymentOptions"] = lblPaymentOptions.Text;
                        //item["InterestRate"] = int.Parse(lblInterestRate.Text);
                        //item["DiscountRate"] = int.Parse(lblDiscountRate.Text);
                        //item["OtherImportantInformation"] = SPHandler.RemoveHTMLTags(lblOtherImportantInformation.Text);
                        //if (!String.IsNullOrEmpty(lblProductPrice.Text))
                        //    item["ProductPrice"] = double.Parse(lblProductPrice.Text);
                        //else
                        //    item["ProductPrice"] = null;

                        //item["Status"] = "Pending Approval";                    

                        item.Update();




                        //try
                        //{
                        //    FileUpload fuAttachDocument = ((FileUpload)repItem.FindControl("fuAttachDocument"));
                        //    File.WriteAllText(@"C:\Temp\fullAttachment.log", fuAttachDocument.ToString());
                        //}
                        //catch (Exception ex)
                        //{
                        //    File.WriteAllText(@"C:\Temp\uploadFileClub0803.log", ex.ToString());
                        //}
                        //if (fuAttachDocument.HasFile)
                        //{
                        //    item.Attachments.AddNow(fuAttachDocument.PostedFile.FileName, fuAttachDocument.FileBytes);
                        //}

                        SendCardExpirationAndAdditionNotificationToAdminAndMembers(item);

                    }
                }

                GoBack();
            });
        }

        private void SendCardExpirationAndAdditionNotificationToAdminAndMembers(SPListItem cardItem)
        {
            string senderEmailSignature = SPUtility.GetLocalizedString("$Resources:SenderEmailSignature", "Club0803_Config", 1033);
            string senderEmailAddress = SPUtility.GetLocalizedString("$Resources:SenderEmailAddress", "Club0803_Config", 1033);
            string superAdminsGroupEmailAddress = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupEmailAddress", "Club0803_Config", 1033);
            string adminsGroupEmailAddress = SPUtility.GetLocalizedString("$Resources:AdminsGroupEmailAddress", "Club0803_Config", 1033);
            // string permanentStaffGroupEmailAddress = SPUtility.GetLocalizedString("$Resources:PermanentStaffGroupEmailAddress", "Club0803_Config", 1033);

            //SPFieldUserValueCollection cardHolderUserValueCollection = new SPFieldUserValueCollection(properties.Web, cardItem["CardHolder"].ToString());
            //SPUser cardHolderUser = cardHolderUserValueCollection[0].User;
            string cardOwner = string.Empty;
            string cardOwnerEmailAddress = string.Empty;
            string cardOwnerPhoneNumber = string.Empty;            
            
            cardOwner = cardItem["Card Holder Upload"].ToString();
            cardOwnerEmailAddress = cardItem["Email Address"].ToString();
            //cardOwnerPhoneNumber = cardItem["Phone Number"].ToString();
            

            DateTime ExpirationDate = ((DateTime)cardItem["ExpiryDate"]);
            string cardNumber = cardItem["Title"] != null ? cardItem["Title"].ToString() : string.Empty;

            ListDictionary lstReplacements = new ListDictionary();
            lstReplacements.Add("<%" + EmailReplacementKeys.ExpirationDate.ToString() + "%>", ExpirationDate.ToString("dd MMM yyyy"));
            lstReplacements.Add("<%" + EmailReplacementKeys.CardNumber.ToString() + "%>", cardNumber);
            lstReplacements.Add("<%" + EmailReplacementKeys.CardOwner.ToString() + "%>", cardOwner);
            lstReplacements.Add("<%" + EmailReplacementKeys.CardOwnerEmailAddress.ToString() + "%>", cardOwnerEmailAddress);

            lstReplacements.Add("<%" + EmailReplacementKeys.SenderEmailSignature.ToString() + "%>", senderEmailSignature);

            string subjectToAdmin = SPUtility.GetLocalizedString("$Resources:CardExpirationNotificationToAdminEmailSubject", "Club0803_Config", 1033);
            string bodyToAdmin = SPUtility.GetLocalizedString("$Resources:CardExpirationNotificationToAdminEmailBody", "Club0803_Config", 1033);

            string subjectToMembers = SPUtility.GetLocalizedString("$Resources:CardExpirationNotificationToMembersEmailSubject", "Club0803_Config", 1033);
            string bodyToMembers = SPUtility.GetLocalizedString("$Resources:CardExpirationNotificationToMembersEmailBody", "Club0803_Config", 1033);

            EMailHandler.AddReminder(subjectToAdmin, bodyToAdmin, ExpirationDate.AddDays(-7), cardItem.UniqueId, adminsGroupEmailAddress, ccList: superAdminsGroupEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);
            EMailHandler.AddReminder(subjectToMembers, bodyToMembers, ExpirationDate.AddDays(-7), cardItem.UniqueId, cardOwnerEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);

            string cardAddedSubjectToMembers = SPUtility.GetLocalizedString("$Resources:CardAddedNotificationToMembersEmailSubject", "Club0803_Config", 1033);
            string cardAddedBodyToMembers = SPUtility.GetLocalizedString("$Resources:CardAddedNotificationToMembersEmailBody", "Club0803_Config", 1033);

            EMailHandler.SendEmail(cardAddedSubjectToMembers, cardAddedBodyToMembers, cardOwnerEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);

            //CardExpirationNotificationToMembersSMS

            string smsBodyToMembers = SPUtility.GetLocalizedString("$Resources:CardExpirationNotificationToMembersSMS", "Club0803_Config", 1033);

            SMSHandler.AddReminder(smsBodyToMembers, ExpirationDate.AddDays(-7), cardItem.UniqueId, "00000000000", replacements: lstReplacements);

        }
    
    }
}

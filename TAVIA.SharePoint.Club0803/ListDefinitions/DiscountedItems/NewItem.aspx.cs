﻿using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using TAVIA.SharePoint.Club0803.Utility;
using TAVIA.SharePoint.Utilities;

namespace TAVIA.SharePoint.Club0803.DiscountedItems
{
    public partial class NewItem : System.Web.UI.Page
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            dtcClosingDate.MinDate = DateTime.Now.AddDays(-1);
            dtcClosingDate.MaxDate = DateTime.Now.AddYears(5);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SPRibbon ribbon = SPRibbon.GetCurrent(this.Page);
                if (ribbon != null)
                {
                    ribbon.Visible = false;
                }
                lblEmployeeName.Text = SPContext.Current.Web.CurrentUser.Name;
                lblEmployeeEmailAddress.Text = SPContext.Current.Web.CurrentUser.Email;
                FillCategories();
                FillTypes(int.Parse(ddlCategory.Text));
                FillVendors();
                FillVendorDetails(int.Parse(ddlVendor.Text));
                //if (!String.IsNullOrEmpty(Page.Request.QueryString["ID"]))
                //{
                //    int itemId = -1;
                //    int.TryParse(Page.Request.QueryString["ID"], out itemId);
                //    SPListItem item = GetSCMSourcingItem(itemId);
                //    if (item != null)
                //    {
                //        FillSavedValues(item);
                //    }
                //}
            }
        }

        private void FillCategories()
        {
            string CategoriesListInternalName = SPUtility.GetLocalizedString("$Resources:CategoriesListUrl", "Club0803_Config", 1033);
            string listUrl = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, CategoriesListInternalName);
            SPList CategoriesList = SPContext.Current.Web.GetList(listUrl);
            ddlCategory.DataSource = CategoriesList.Items.GetDataTable();
            ddlCategory.DataValueField = "ID";
            ddlCategory.DataTextField = "Title";
            ddlCategory.DataBind();
        }

        private void FillTypes(int categoryID)
        {
            //ddlDepartment.Items.Clear();
            string CategoryTypesListInternalName = SPUtility.GetLocalizedString("$Resources:CategoryTypesListUrl", "Club0803_Config", 1033);
            string listUrl = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, CategoryTypesListInternalName);
            SPList CategoryTypesList = SPContext.Current.Web.GetList(listUrl);
            SPQuery query = new SPQuery();
            query.Query = String.Format("<Where><Eq><FieldRef Name=\"{0}\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{1}</Value></Eq></Where>", CategoryTypesList.Fields["Category"].InternalName, categoryID);
            ddlType.DataSource = CategoryTypesList.GetItems(query).GetDataTable();
            ddlType.DataValueField = "ID";
            ddlType.DataTextField = "Title";
            ddlType.DataBind();
        }

        private void FillVendors()
        {
            string VendorsListInternalName = SPUtility.GetLocalizedString("$Resources:VendorsListUrl", "Club0803_Config", 1033);
            string listUrl = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, VendorsListInternalName);
            SPList VendorsList = SPContext.Current.Web.GetList(listUrl);
            ddlVendor.DataSource = VendorsList.Items.GetDataTable();
            ddlVendor.DataValueField = "ID";
            ddlVendor.DataTextField = "Title";
            ddlVendor.DataBind();
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillTypes(int.Parse(ddlCategory.Text));
        }

        protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillVendorDetails(int.Parse(ddlVendor.Text));
        }

        private void FillVendorDetails(int vendorID)
        {
            string VendorsListInternalName = SPUtility.GetLocalizedString("$Resources:VendorsListUrl", "Club0803_Config", 1033);
            string listUrl = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, VendorsListInternalName);
            SPList VendorsList = SPContext.Current.Web.GetList(listUrl);
            SPListItem vendorItem = VendorsList.GetItemById(vendorID);

            if (vendorItem["PhoneNumber"] != null)
            {
                lblVendorPhoneNumber.Text = vendorItem["PhoneNumber"].ToString();
            }

            if (vendorItem["Email"] != null)
            {
                lblVendorContactEmail.Text = vendorItem["Email"].ToString();
            }

            if (vendorItem["ContactPerson"] != null)
            {
                lblVendorContactPerson.Text = vendorItem["ContactPerson"].ToString();
            }

            if (vendorItem["ContactAddress"] != null)
            {
                lblVendorContactAddress.Text = vendorItem["ContactAddress"].ToString().Replace("\r\n", "<br />"); ;
            }
        }


        //private void FillSavedValues(SPListItem item)
        //{
        //    if (item != null)
        //    {
        //        SPFieldUserValueCollection employeeUserValueCollection = new SPFieldUserValueCollection(SPContext.Current.Web, item["Author"].ToString());
        //        SPUser employeeUser = employeeUserValueCollection[0].User;

        //        lblEmployeeName.Text = employeeUser.Name;
        //        lblEmployeeEmailAddress.Text = employeeUser.Email;

        //        if (item["MobileNumber"] != null)
        //            txtMobileNumber.Text = item["MobileNumber"].ToString();
        //        if (item["Designation"] != null)
        //            txtDesignation.Text = item["Designation"].ToString();
        //        if (item["Division"] != null)
        //        {
        //            SPFieldLookupValue division = new SPFieldLookupValue(item["Division"].ToString());
        //            ddlDivision.Text = division.LookupId.ToString();
        //        }
        //        if (item["Location"] != null)
        //        {
        //            SPFieldLookupValue location = new SPFieldLookupValue(item["Location"].ToString());
        //            ddlLocation.Text = location.LookupId.ToString();
        //        }

        //        if (item["IsOPEXorCAPEX"] != null)
        //            rblIsOPEXorCAPEX.Text = item["IsOPEXorCAPEX"].ToString();

        //        if (item["Details"] != null)
        //            txtDetails.Text = item["Details"].ToString();
        //        if (item["IsCAPEXApproved"] != null)
        //        {
        //            bool isCAPEXApproved = (bool)item["IsCAPEXApproved"];
        //            rblIsCAPEXApproved.Text = isCAPEXApproved.ToString();
        //            if (isCAPEXApproved)
        //            {
        //                if (item["ProjectCode"] != null)
        //                    txtProjectCode.Text = item["ProjectCode"].ToString();
        //            }
        //        }

        //        if (item["RequiredQuantity"] != null)
        //            txtRequiredQuantity.Text = item["RequiredQuantity"].ToString();

        //        if (item["ProductDescription"] != null)
        //            txtProductDescription.Text = item["ProductDescription"].ToString();

        //        if (item["DeliveryLocation"] != null)
        //            txtDeliveryLocation.Text = item["DeliveryLocation"].ToString();

        //        if (item["BudgetedAmountCurrency"] != null)
        //            ddlBudgetedAmountCurrency.Text = item["BudgetedAmountCurrency"].ToString();

        //        if (item["ExpectedCostOrBudget"] != null)
        //            txtExpectedCostOrBudget.Text = String.Format("{0:n}", (double)item["ExpectedCostOrBudget"]);

        //        if (item["IsItemBoughtPreviously"] != null)
        //        {
        //            bool isItemBoughtPreviously = (bool)item["IsItemBoughtPreviously"];
        //            rblIsItemBoughtPreviously.Text = isItemBoughtPreviously.ToString();
        //            if (isItemBoughtPreviously)
        //            {
        //                if (item["ItemPurchaseDate"] != null)
        //                    dtcItemPurchaseDate.SelectedDate = (DateTime)item["ItemPurchaseDate"];

        //                if (item["PurchasePrice"] != null)
        //                    txtPurchasePrice.Text = String.Format("{0:n}", (double)item["PurchasePrice"]);
        //            }
        //        }

        //        if (item["SuggestedClosingDate"] != null)
        //            dtcSuggestedClosingDate.SelectedDate = (DateTime)item["SuggestedClosingDate"];

        //        if (item["PartsInStock"] != null)
        //        {
        //            bool partsInStock = (bool)item["PartsInStock"];
        //            rblPartsInStock.Text = partsInStock.ToString();
        //        }

        //        if (item["SuggestedVendorName"] != null)
        //            txtSuggestedVendorName.Text = item["SuggestedVendorName"].ToString();

        //        if (item["SuggestedVendorPhoneNumber"] != null)
        //            txtSuggestedVendorPhoneNumber.Text = item["SuggestedVendorPhoneNumber"].ToString();

        //        if (item["SuggestedVendorEmailAddress"] != null)
        //            txtSuggestedVendorEmailAddress.Text = item["SuggestedVendorEmailAddress"].ToString();

        //        if (item["Justification"] != null)
        //            txtJustification.Text = item["Justification"].ToString();

        //        if (item["PreferSingleSupplier"] != null)
        //        {
        //            bool preferSingleSupplier = (bool)item["PreferSingleSupplier"];
        //            rblPreferSingleSupplier.Text = preferSingleSupplier.ToString();
        //        }

        //        if (item["WarrantyOrSupportPeriod"] != null)
        //        {
        //            string warranty = item["WarrantyOrSupportPeriod"].ToString();
        //            if (warranty.Contains("Month"))
        //            {
        //                ddlWarrantyOrSupportPeriod.Text = "Month";
        //            }
        //            else if (warranty.Contains("Year"))
        //            {
        //                ddlWarrantyOrSupportPeriod.Text = "Year";
        //            }
        //            txtWarrantyOrSupportPeriod.Text = warranty.Split(' ')[0];
        //        }

        //        if (item["IsInlineWithBusinessPlan"] != null)
        //        {
        //            bool isInlineWithBusinessPlan = (bool)item["IsInlineWithBusinessPlan"];
        //            rblIsInlineWithBusinessPlan.Text = isInlineWithBusinessPlan.ToString();
        //        }

        //        if (item["IsBuyBackRequired"] != null)
        //        {
        //            bool isBuyBackRequired = (bool)item["IsBuyBackRequired"];
        //            rblIsBuyBackRequired.Text = isBuyBackRequired.ToString();
        //        }

        //        if (item["IsSiteInspectionRequired"] != null)
        //        {
        //            bool isSiteInspectionRequired = (bool)item["IsSiteInspectionRequired"];
        //            rblIsSiteInspectionRequired.Text = isSiteInspectionRequired.ToString();
        //        }

        //        if (item["SiteInspectionDate"] != null)
        //            dtcSiteInspectionDate.SelectedDate = (DateTime)item["SiteInspectionDate"];

        //        if (item["SiteInspectionContactPerson"] != null)
        //            txtSiteInspectionContactPerson.Text = item["SiteInspectionContactPerson"].ToString();

        //        if (item["SiteInspectionContactNumber"] != null)
        //            txtSiteInspectionContactNumber.Text = item["SiteInspectionContactNumber"].ToString();

        //        if (item["SiteInspectionContactEmail"] != null)
        //            txtSiteInspectionContactEmail.Text = item["SiteInspectionContactEmail"].ToString();

        //        if (item["OtherInformation"] != null)
        //            txtOtherInformation.Text = item["OtherInformation"].ToString();

        //        if (item["BudgetAccountant"] != null)
        //        {
        //            SPFieldUserValueCollection BudgetAccountantValueCollection = new SPFieldUserValueCollection(SPContext.Current.Web, item["BudgetAccountant"].ToString());
        //            SPUser BudgetAccountantUser = BudgetAccountantValueCollection[0].User;
        //            peBudgetAccountant.CommaSeparatedAccounts = BudgetAccountantUser.LoginName;
        //            peBudgetAccountant.Validate();
        //        }

        //        if (item["LineManager"] != null)
        //        {
        //            SPFieldUserValueCollection LineManagerValueCollection = new SPFieldUserValueCollection(SPContext.Current.Web, item["LineManager"].ToString());
        //            SPUser LineManagerUser = LineManagerValueCollection[0].User;
        //            peLineManager.CommaSeparatedAccounts = LineManagerUser.LoginName;
        //            peLineManager.Validate();
        //        }

        //        if (item["BudgetApprover"] != null)
        //        {
        //            SPFieldUserValueCollection BudgetApproverValueCollection = new SPFieldUserValueCollection(SPContext.Current.Web, item["BudgetApprover"].ToString());
        //            SPUser BudgetApproverUser = BudgetApproverValueCollection[0].User;
        //            peBudgetApprover.CommaSeparatedAccounts = BudgetApproverUser.LoginName;
        //            peBudgetApprover.Validate();
        //        }


        //        //    if (item.Attachments.Count > 0)
        //        //    {
        //        //        hlMarriageCertificte.NavigateUrl = item.Attachments.UrlPrefix + item.Attachments[0];
        //        //    }
        //        //    else
        //        //    {
        //        //        hlMarriageCertificte.Visible = false;
        //        //    }
        //    }
        //}

        //private SPListItem GetSCMSourcingItem(int itemId)
        //{
        //    SPListItem scmSourcingItem = null;
        //    try
        //    {
        //        scmSourcingItem = SPContext.Current.List.GetItemById(itemId);
        //    }
        //    catch
        //    {
        //        GoBack();
        //    }
        //    if (scmSourcingItem != null)
        //    {
        //        if (scmSourcingItem["Title"] != null)
        //        {
        //            string status = scmSourcingItem["Title"].ToString();
        //            if (status != "Draft")//if the item is not saved
        //            {
        //                GoBack();
        //            }
        //        }
        //        SPFieldUserValueCollection employeeUserValueCollection = new SPFieldUserValueCollection(SPContext.Current.Web, scmSourcingItem["Author"].ToString());
        //        SPUser employeeUser = employeeUserValueCollection[0].User;
        //        if (SPContext.Current.Web.CurrentUser.ID != employeeUser.ID)
        //        {
        //            GoBack();
        //        }
        //    }
        //    return scmSourcingItem;
        //}

        //private void FillDivisions()
        //{
        //    ddlDivision.DataSource = SPHandler.GetDivisions(SPContext.Current.Site.RootWeb);
        //    ddlDivision.DataValueField = "ID";
        //    ddlDivision.DataTextField = "Title";
        //    ddlDivision.DataBind();
        //}
        //private void FillLocations()
        //{
        //    ddlLocation.DataSource = SPHandler.GetLocations(SPContext.Current.Site.RootWeb);
        //    ddlLocation.DataValueField = "ID";
        //    ddlLocation.DataTextField = "Title";
        //    ddlLocation.DataBind();
        //}
        //private void FillCurrencies()
        //{
        //    StringCollection colCurrencies = ((SPFieldMultiChoice)SPContext.Current.List.Fields.GetFieldByInternalName("BudgetedAmountCurrency")).Choices;
        //    foreach (string access in colCurrencies)
        //    {
        //        ddlBudgetedAmountCurrency.Items.Add(access);
        //    }
        //}

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            GoBack();
        }

        private void GoBack()
        {
            if (Page.Request.QueryString["IsDlg"] != null)
            {
                HttpContext context = HttpContext.Current;
                context.Response.Write("<script type='text/javascript'>window.frameElement.cancelPopUp()</script>");
                context.Response.Flush();
                context.Response.End();
            }
            else if (!String.IsNullOrEmpty(Page.Request.QueryString["Source"]))
            {
                Response.Redirect(Page.Request.QueryString["Source"].ToString());
            }
            else
            {
                string DiscountedItemsListInternalName = SPUtility.GetLocalizedString("$Resources:DiscountedItemsListUrl", "Club0803_Config", 1033);
                string url = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, DiscountedItemsListInternalName);
                Response.Redirect(url);
                //Response.Redirect(SPContext.Current.Web.Url);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;

            SPListItem item = null;
            bool isNew = false;
            //if (!String.IsNullOrEmpty(Page.Request.QueryString["ID"]))
            //{
            //    int itemId = -1;
            //    int.TryParse(Page.Request.QueryString["ID"], out itemId);
            //    item = GetSCMSourcingItem(itemId);
            //}
            if (item == null)
            {
                item = SPContext.Current.List.AddItem();
                isNew = true;
            }

            item["Title"] = SPHandler.RemoveHTMLTags(txtProductName.Text);
            item["Category"] = new SPFieldLookupValue(int.Parse(ddlCategory.Text), ddlCategory.SelectedItem.Text);
            item["CategoryType"] = new SPFieldLookupValue(int.Parse(ddlType.Text), ddlType.SelectedItem.Text);
            item["VendorName"] = new SPFieldLookupValue(int.Parse(ddlVendor.Text), ddlVendor.SelectedItem.Text);
            item["ProductDescription"] = SPHandler.RemoveHTMLTags(txtProductDescription.Text);
            if (!String.IsNullOrEmpty(txtVendorProductWebsiteURL.Text))
                item["VendorProductWebsiteURL"] = SPHandler.RemoveHTMLTags(txtVendorProductWebsiteURL.Text);
            else
                item["VendorProductWebsiteURL"] = null;
            item["ClosingDate"] = dtcClosingDate.SelectedDate;
            item["PaymentOptions"] = rblPaymentOptions.Text;
            item["InterestRate"] = int.Parse(txtInterestRate.Text);
            item["DiscountRate"] = int.Parse(txtDiscountRate.Text);
            item["OtherImportantInformation"] = SPHandler.RemoveHTMLTags(txtOtherImportantInformation.Text);
            if (!String.IsNullOrEmpty(txtProductPrice.Text))
                item["ProductPrice"] = double.Parse(txtProductPrice.Text);
            else
                item["ProductPrice"] = null;
            item["Status"] = "Pending Approval";

            item.Update();

            if (fuAttachDocument.HasFile)
            {
                item.Attachments.AddNow(fuAttachDocument.PostedFile.FileName, fuAttachDocument.FileBytes);
            }

            SendDiscountedItemUploadNotification(item);

            GoBack();
        }

        private void SendDiscountedItemUploadNotification(SPListItem discountedItem)
        {
            string senderEmailSignature = SPUtility.GetLocalizedString("$Resources:SenderEmailSignature", "Club0803_Config", 1033);
            string senderEmailAddress = SPUtility.GetLocalizedString("$Resources:SenderEmailAddress", "Club0803_Config", 1033);
            string superAdminsGroupEmailAddress = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupEmailAddress", "Club0803_Config", 1033);
            string adminsGroupEmailAddress = SPUtility.GetLocalizedString("$Resources:AdminsGroupEmailAddress", "Club0803_Config", 1033);

            //string TribesListInternalName = SPUtility.GetLocalizedString("$Resources:Lists_Tribes_ListUrl", "InnovationPortal_Config", 1033);
            //string TribesListUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, TribesListInternalName);
            //SPList TribesList = impersonatedWeb.GetList(TribesListUrl);
            //SPListItem tribeItem = TribesList.GetItemById(tribeID);
            //String tribeName = tribeItem["Title"] != null ? tribeItem["Title"].ToString() : string.Empty;
            //SPFieldUserValueCollection createdByUserValueCollection = new SPFieldUserValueCollection(impersonatedWeb, tribeItem["Author"].ToString());
            //SPUser createdByUser = createdByUserValueCollection[0].User;
            //String createdBy = createdByUser.Name;
            //String createdDate = ((DateTime)tribeItem["Created"]).ToString("dd MMM yyyy");

            ListDictionary lstReplacements = new ListDictionary();
            lstReplacements.Add("<%" + EmailReplacementKeys.DiscountedItemNumber.ToString() + "%>", discountedItem.ID.ToString());
            string discountedItemUrl = discountedItem.ParentList.ParentWeb.Site.MakeFullUrl(discountedItem.ParentList.DefaultDisplayFormUrl) + "?ID=" + discountedItem.ID.ToString();
            lstReplacements.Add("<%" + EmailReplacementKeys.DiscountedItemUrl.ToString() + "%>", "<a href='" + discountedItemUrl + "'>click here</a>");
            lstReplacements.Add("<%" + EmailReplacementKeys.SenderEmailSignature.ToString() + "%>", senderEmailSignature);

            string subjectToAdmin = SPUtility.GetLocalizedString("$Resources:DiscountedItemUploadNotificationToAdminEmailSubject", "Club0803_Config", 1033);
            string bodyToAdmin = SPUtility.GetLocalizedString("$Resources:DiscountedItemUploadNotificationToAdminEmailBody", "Club0803_Config", 1033);

            string subjectToSuperAdmin = SPUtility.GetLocalizedString("$Resources:DiscountedItemUploadNotificationToSuperAdminEmailSubject", "Club0803_Config", 1033);
            string bodyToSuperAdmin = SPUtility.GetLocalizedString("$Resources:DiscountedItemUploadNotificationToSuperAdminEmailBody", "Club0803_Config", 1033);

            EMailHandler.SendEmail(subjectToAdmin, bodyToAdmin, adminsGroupEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);
            EMailHandler.SendEmail(subjectToSuperAdmin, bodyToSuperAdmin, superAdminsGroupEmailAddress, fromAddress: senderEmailAddress, replacements: lstReplacements);
            
        }

        //protected void btnSave_Click(object sender, EventArgs e)
        //{
        //    if (!Page.IsValid)
        //        return;

        //    List<SPUserRole> userRoles = new List<SPUserRole>();

        //    SPListItem item = null;
        //    bool isNew = false;
        //    if (!String.IsNullOrEmpty(Page.Request.QueryString["ID"]))
        //    {
        //        int interviewID = -1;
        //        int.TryParse(Page.Request.QueryString["ID"], out interviewID);
        //        item = GetSCMSourcingItem(interviewID);
        //    }
        //    if (item == null)
        //    {
        //        item = SPContext.Current.List.AddItem();
        //        isNew = true;
        //    }

        //    if (!IsPeoplePickersValid(true))
        //        return;

        //    EnsureUsers();

        //    item["MobileNumber"] = SPHandler.RemoveHTMLTags(txtMobileNumber.Text);
        //    item["Designation"] = SPHandler.RemoveHTMLTags(txtDesignation.Text);
        //    item["Division"] = new SPFieldLookupValue(int.Parse(ddlDivision.Text), ddlDivision.SelectedItem.Text);
        //    item["Location"] = new SPFieldLookupValue(int.Parse(ddlLocation.Text), ddlLocation.SelectedItem.Text);
        //    item["IsOPEXorCAPEX"] = rblIsOPEXorCAPEX.Text;
        //    item["Details"] = SPHandler.RemoveHTMLTags(txtDetails.Text);
        //    if (!String.IsNullOrEmpty(rblIsCAPEXApproved.Text))
        //        item["IsCAPEXApproved"] = bool.Parse(rblIsCAPEXApproved.Text);
        //    if (rblIsCAPEXApproved.Items[0].Selected)
        //    {
        //        item["ProjectCode"] = SPHandler.RemoveHTMLTags(txtProjectCode.Text);
        //    }
        //    else
        //    {
        //        item["ProjectCode"] = null;
        //    }

        //    item["RequiredQuantity"] = SPHandler.RemoveHTMLTags(txtRequiredQuantity.Text);
        //    item["ProductDescription"] = SPHandler.RemoveHTMLTags(txtProductDescription.Text);
        //    item["DeliveryLocation"] = SPHandler.RemoveHTMLTags(txtDeliveryLocation.Text);
        //    item["BudgetedAmountCurrency"] = ddlBudgetedAmountCurrency.Text;
        //    item["ExpectedCostOrBudget"] = double.Parse(txtExpectedCostOrBudget.Text);
        //    if (!String.IsNullOrEmpty(rblIsItemBoughtPreviously.Text))
        //    {
        //        bool isItemBoughtPreviously = bool.Parse(rblIsItemBoughtPreviously.Text);
        //        item["IsItemBoughtPreviously"] = isItemBoughtPreviously;
        //        if (isItemBoughtPreviously && !dtcItemPurchaseDate.IsDateEmpty)
        //        {
        //            item["ItemPurchaseDate"] = dtcItemPurchaseDate.SelectedDate;
        //        }
        //        else
        //        {
        //            item["ItemPurchaseDate"] = null;
        //        }
        //        if (isItemBoughtPreviously && !String.IsNullOrEmpty(txtPurchasePrice.Text))
        //        {
        //            item["PurchasePrice"] = double.Parse(txtPurchasePrice.Text);
        //        }
        //        else
        //        {
        //            item["PurchasePrice"] = null;
        //        }
        //    }
        //    if (!dtcSuggestedClosingDate.IsDateEmpty)
        //        item["SuggestedClosingDate"] = dtcSuggestedClosingDate.SelectedDate;
        //    if (!String.IsNullOrEmpty(rblPartsInStock.Text))
        //    {
        //        item["PartsInStock"] = bool.Parse(rblPartsInStock.Text);
        //    }
        //    else
        //    {
        //        item["PartsInStock"] = null;
        //    }
        //    if (!String.IsNullOrEmpty(txtSuggestedVendorName.Text))
        //    {
        //        item["SuggestedVendorName"] = SPHandler.RemoveHTMLTags(txtSuggestedVendorName.Text);
        //    }
        //    else
        //    {
        //        item["SuggestedVendorName"] = null;
        //    }
        //    if (!String.IsNullOrEmpty(txtSuggestedVendorPhoneNumber.Text))
        //    {
        //        item["SuggestedVendorPhoneNumber"] = SPHandler.RemoveHTMLTags(txtSuggestedVendorPhoneNumber.Text);
        //    }
        //    else
        //    {
        //        item["SuggestedVendorPhoneNumber"] = null;
        //    }
        //    if (!String.IsNullOrEmpty(txtSuggestedVendorEmailAddress.Text))
        //    {
        //        item["SuggestedVendorEmailAddress"] = SPHandler.RemoveHTMLTags(txtSuggestedVendorEmailAddress.Text);
        //    }
        //    else
        //    {
        //        item["SuggestedVendorEmailAddress"] = null;
        //    }

        //    if (!String.IsNullOrEmpty(txtJustification.Text))
        //    {
        //        item["Justification"] = SPHandler.RemoveHTMLTags(txtJustification.Text);
        //    }
        //    else
        //    {
        //        item["Justification"] = null;
        //    }

        //    if (!String.IsNullOrEmpty(rblPreferSingleSupplier.Text))
        //    {
        //        item["PreferSingleSupplier"] = bool.Parse(rblPreferSingleSupplier.Text);
        //    }
        //    else
        //    {
        //        item["PreferSingleSupplier"] = null;
        //    }

        //    if (!String.IsNullOrEmpty(txtWarrantyOrSupportPeriod.Text))
        //    {
        //        int warrantyAmount = 0;
        //        int.TryParse(txtWarrantyOrSupportPeriod.Text, out warrantyAmount);
        //        if (warrantyAmount > 1)
        //            item["WarrantyOrSupportPeriod"] = String.Format("{0} {1}s", txtWarrantyOrSupportPeriod.Text, ddlWarrantyOrSupportPeriod.Text);
        //        else
        //            item["WarrantyOrSupportPeriod"] = String.Format("{0} {1}", txtWarrantyOrSupportPeriod.Text, ddlWarrantyOrSupportPeriod.Text);
        //    }
        //    else
        //    {
        //        item["WarrantyOrSupportPeriod"] = null;
        //    }

        //    if (!String.IsNullOrEmpty(rblIsInlineWithBusinessPlan.Text))
        //    {
        //        item["IsInlineWithBusinessPlan"] = bool.Parse(rblIsInlineWithBusinessPlan.Text);
        //    }
        //    else
        //    {
        //        item["IsInlineWithBusinessPlan"] = null;
        //    }

        //    if (!String.IsNullOrEmpty(rblIsBuyBackRequired.Text))
        //    {
        //        item["IsBuyBackRequired"] = bool.Parse(rblIsBuyBackRequired.Text);
        //    }
        //    else
        //    {
        //        item["IsBuyBackRequired"] = null;
        //    }

        //    if (!String.IsNullOrEmpty(rblIsSiteInspectionRequired.Text))
        //    {
        //        item["IsSiteInspectionRequired"] = bool.Parse(rblIsSiteInspectionRequired.Text);
        //    }
        //    else
        //    {
        //        item["IsSiteInspectionRequired"] = null;
        //    }

        //    if (rblIsSiteInspectionRequired.Items[0].Selected && !dtcSiteInspectionDate.IsDateEmpty)
        //    {
        //        item["SiteInspectionDate"] = dtcSiteInspectionDate.SelectedDate;
        //    }
        //    else
        //    {
        //        item["SiteInspectionDate"] = null;
        //    }

        //    if (!String.IsNullOrEmpty(txtSiteInspectionContactPerson.Text))
        //    {
        //        item["SiteInspectionContactPerson"] = SPHandler.RemoveHTMLTags(txtSiteInspectionContactPerson.Text);
        //    }
        //    else
        //    {
        //        item["SiteInspectionContactPerson"] = null;
        //    }

        //    if (!String.IsNullOrEmpty(txtSiteInspectionContactNumber.Text))
        //    {
        //        item["SiteInspectionContactNumber"] = SPHandler.RemoveHTMLTags(txtSiteInspectionContactNumber.Text);
        //    }
        //    else
        //    {
        //        item["SiteInspectionContactNumber"] = null;
        //    }

        //    if (!String.IsNullOrEmpty(txtSiteInspectionContactEmail.Text))
        //    {
        //        item["SiteInspectionContactEmail"] = SPHandler.RemoveHTMLTags(txtSiteInspectionContactEmail.Text);
        //    }
        //    else
        //    {
        //        item["SiteInspectionContactEmail"] = null;
        //    }

        //    if (!String.IsNullOrEmpty(txtOtherInformation.Text))
        //    {
        //        item["OtherInformation"] = SPHandler.RemoveHTMLTags(txtOtherInformation.Text);
        //    }
        //    else
        //    {
        //        item["OtherInformation"] = null;
        //    }

        //    if (_BudgetAccountantUser != null)
        //    {
        //        SPFieldUserValue BudgetAccountantUserValue = new SPFieldUserValue(SPContext.Current.Web, _BudgetAccountantUser.ID, _BudgetAccountantUser.LoginName);
        //        item["BudgetAccountant"] = BudgetAccountantUserValue;
        //    }
        //    else
        //    {
        //        item["BudgetAccountant"] = null;
        //    }

        //    if (_LineManagerUser != null)
        //    {
        //        SPFieldUserValue LineManagerUserValue = new SPFieldUserValue(SPContext.Current.Web, _LineManagerUser.ID, _LineManagerUser.LoginName);
        //        item["LineManager"] = LineManagerUserValue;
        //    }
        //    else
        //        item["LineManager"] = null;

        //    if (_BudgetApproverUser != null)
        //    {
        //        SPFieldUserValue BudgetApproverUserValue = new SPFieldUserValue(SPContext.Current.Web, _BudgetApproverUser.ID, _BudgetApproverUser.LoginName);
        //        item["BudgetApprover"] = BudgetApproverUserValue;
        //    }
        //    else
        //    {
        //        item["BudgetApprover"] = null;
        //    }

        //    Save
        //    item["Title"] = "Draft";

        //    item.Update();

        //    if (isNew)
        //    {
        //        userRoles.Add(new SPUserRole(SPContext.Current.Web.CurrentUser.LoginName, SPRoleType.Contributor, false));

        //        WorkflowHandler.SetListItemSecurity(item, userRoles, false);
        //    }

        //    string applicationNumber = string.Format("{0}{1:00000}", "Request_Ex", item.ID);

        //    item.SystemUpdate();

        //    if (isSpouseSIMSelected && fuAttachDocument.HasFile)
        //    {
        //        item.Attachments.AddNow(fuAttachDocument.PostedFile.FileName, fuAttachDocument.FileBytes);
        //    }

        //    GoBack();

        //    GoBack();
        //}

        //protected void cvProjectCode_ServerSide(object sender, ServerValidateEventArgs e)
        //{
        //    if (rblIsCAPEXApproved.Items[0].Selected && String.IsNullOrEmpty(txtProjectCode.Text))
        //    {
        //        e.IsValid = false;
        //    }
        //    else
        //    {
        //        e.IsValid = true;
        //    }
        //}

        //protected void cvAttachDocumentRequired_ServerSide(object sender, ServerValidateEventArgs e)
        //{
        //    if (fuAttachDocument.HasFile || !String.IsNullOrEmpty(txtDetails.Text))
        //    {
        //        e.IsValid = true;
        //    }
        //    else
        //    {
        //        e.IsValid = false;
        //    }
        //}
    }
}

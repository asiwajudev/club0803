﻿<%@ Assembly Name="TAVIA.SharePoint.Club0803, Version=1.0.0.0, Culture=neutral, PublicKeyToken=e94667b04745ceb1" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewDiscountedItems.aspx.cs" Inherits="TAVIA.SharePoint.Club0803.SitePagesModule.ViewDiscountedItems" MasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <style>
        #sideNavBox {
            display: none;
        }

        #contentBox {
            margin-right: 20px;
            margin-left: 20px;
        }

        .h4, h4 {
            font-size: 1.5rem;
        }
        #s4-ribbonrow{
			display:none !important;
		}
		#s4-titlerow{
			display:none !important;
		}
		.ms-core-pageTitle{
			display:none !important;
        }

    </style>

    <SharePoint:UIVersionedContent ID="UIVersionedContent1" UIVersion="4" runat="server">
        <contenttemplate>
		<SharePoint:CssRegistration Name="forms.css" runat="server"/>
	</contenttemplate>
    </SharePoint:UIVersionedContent>

    <link href="/_layouts/15/club0803/styles/bootstrap.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-grid.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-reboot.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/style.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/responsive.dataTables.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/owl.carousel.min.css" rel="stylesheet" />

    <script src="/_layouts/15/club0803/js/jquery.min.js"></script>
    <script src="/_layouts/15/club0803/js/bootstrap.min.js"></script>
    <script src="/_layouts/15/club0803/js/script.js"></script>
    <script src="/_layouts/15/club0803/js/jquery.dataTables.min.js"></script>
    <script src="/_layouts/15/club0803/js/dataTables.responsive.min.js"></script>
    <script src="/_layouts/15/club0803/js/owl.carousel.min.js"></script>

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <section class="col-md-12 form-container pb-3">
        <h2 class="title-container text-center py-3">Discounted Items</h2>
        <div class="form-bg">
            <div class="container">
                <div class="row pt-3">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="form-group">
                            <label for="ddlVendor">Vendor</label>
                            <asp:DropDownList ID="ddlVendor" class="form-control mtn-input" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="form-group">
                            <label for="ddlCategory">Category</label>
                            <asp:DropDownList ID="ddlCategory" class="form-control mtn-input" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="form-group">
                            <label for="txtProductName">Product Name</label>
                            <SharePoint:InputFormTextBox ID="txtProductName" RichText="false" TextMode="Singleline"
                                runat="server" MaxLength="128" title="Product Name" class="form-control mtn-input mtn-textbox" />
                        </div>
                    </div>
                </div>
                
                <div class="row pt-3">
                    <div class="col-md-12 col-sm-12 col-12 pb-3">
                        <div class="row">
                            <div class="col-12 text-left">
                                <asp:Button ID="btnSearch" class="btn mtn-btn mr-3" runat="server" Text="Search" OnClick="btnSearch_Click" />
                                <asp:Button ID="btnClearSearch" class="btn mtn-btn mr-3" runat="server" Text="Reset" OnClick="btnClearSearch_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <section class="col-md-12 py-0 productlist-container py-4">
                        <div class="container shoplist-pad">
                             <h4 class="text-center font-weight-bold">Latest Deals</h4>
                            <div class="owl-carousel owl-theme owl-img-responsive">
                                <asp:Repeater ID="repLatestDiscountedItems" runat="server">
                                   <ItemTemplate>
                                       <div class="item">
                                            <div class="card border-0 product-card">                                                            
                                                <img class="card-img-top" src='<%# DataBinder.Eval(Container.DataItem, "ImgUrl") %>' alt="card-img-top">
                                                <div class="card-body text-center px-2">
                                                <p class="font-weight-bold"><%# DataBinder.Eval(Container.DataItem, "Title") %></p>
                                                <p><%# DataBinder.Eval(Container.DataItem, "ClosingDate") %></p>                              
                                                    <a href='ViewDiscountedItem.aspx?ID=<%# DataBinder.Eval(Container.DataItem, "ID") %>' class="btn mtn-btn">View Details</a>                                    
                                                </div>                                 
                                            </div>
                                        </div>
                                   </ItemTemplate>
                              </asp:Repeater>
                            </div>
                            <span id="dots"></span>

                            <div class="py-4 px-5 col-md-12" id="more">                   
                                <div class="row">
                                    <asp:Repeater ID="repDiscountedItems" runat="server">
                                         <ItemTemplate>
                                             <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3">                        
                                                <div class="card border-0 product-card">                                                            
                                                        <img class="card-img-top" src='<%# DataBinder.Eval(Container.DataItem, "ImgUrl") %>' alt="card-img-top">
                                                        <div class="card-body text-center px-2">
                                                        <p class="font-weight-bold"><%# DataBinder.Eval(Container.DataItem, "Title") %></p>
                                                        <p><%# DataBinder.Eval(Container.DataItem, "ClosingDate") %></p>                              
                                                            <a href='ViewDiscountedItem.aspx?ID=<%# DataBinder.Eval(Container.DataItem, "ID") %>' class="btn mtn-btn">View Details</a>                                     
                                                        </div>                                 
                                                </div>                       
                                            </div>
                                         </ItemTemplate>
                                    </asp:Repeater>                
                                </div>
                                </div>   

                            <!-- <div  class="col-md-12 text-center">View All</div> -->
                            <div class="col-md-12 text-center"><button onclick="myFunction();return false;" id="myBtn"><i class="fa fa-chevron-down"></i></button></div>
                        </div>
                    </section>
                                        
            </div>

            <div class="col-md-12">
                <hr class="mtn-divider m-0 px-3" />
            </div>
            <!-- button section -->
            <div class="col-md-12 py-3">
                <div class="row btn-div text-center">
                    <asp:Button ID="btnCancel" class="btn mtn-btn mr-3" CausesValidation="false" runat="server" Text="Go to Home" OnClick="btnCancel_Click" />
                </div>
            </div>
        </div>
    </section>

    <script>

        // quick links
        $('.productlist-container .owl-carousel').owlCarousel({
            loop: true,
            margin: 20,
            nav: false,
            autoplay: true,
            dots: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                },
                1280: {
                    items: 3
                },
                1366: {
                    items: 3
                },
                1600: {
                    items: 3
                },
            }
        })

        // view more script
        function myFunction() {
            var dots = document.getElementById("dots");
            var moreText = document.getElementById("more");
            var btnText = document.getElementById("myBtn");

            if (dots.style.display === "none") {
                dots.style.display = "inline";
                btnText.innerHTML = "<i class='fa fa-chevron-down'></i>";
                moreText.style.display = "none";
            } else {
                dots.style.display = "none";
                btnText.innerHTML = "<i class='fa fa-chevron-up'></i>";
                moreText.style.display = "inline";
            }
        }

    </script>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Club0803 - Discounted Items
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" />

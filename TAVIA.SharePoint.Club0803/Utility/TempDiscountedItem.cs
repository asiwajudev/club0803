﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAVIA.SharePoint.Club0803
{
    [Serializable]
    public class TempDiscountedItem
    {

        public int Id { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public string VendorProductWebsiteURL { get; set; }
        public string ClosingDate { get; set; }
        public string PaymentOptions { get; set; }
        public int InterestRate { get; set; }
        public int DiscountRate { get; set; }
        public string OtherImportantInformation { get; set; }
        public string ProductPrice { get; set; }
       // public string Status { get; set; }

        //public bool IsNew { get; set; }
    }

    public class MultipleCardUploads
    {

        public int Id { get; set; }
        public string CardNumber { get; set; }
        public string CardHolder { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string IssueDate { get; set; }
        public string ExpiryDate { get; set; }
        public string CardType { get; set; }
        //public int DiscountRate { get; set; }
        //public string OtherImportantInformation { get; set; }
        //public string ProductPrice { get; set; }
        // public string Status { get; set; }

        //public bool IsNew { get; set; }
    }
}

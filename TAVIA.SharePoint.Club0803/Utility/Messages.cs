﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAVIA.SharePoint.Club0803.Utility
{
    public enum Messages
    {
        Disocunted_Item_Does_Not_Exist_Or_Expired,
        Order_Submitted_Successfully,
        Vendor_Suggestion_Submitted_Successfully,
        Card_Requested_Successfully,
        Rating_Submitted_Successfully
    }
}

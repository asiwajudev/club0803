﻿using System;
using System.Web;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.WebPartPages;

namespace TAVIA.SharePoint.Club0803.SitePagesModule
{
    public partial class MyOrders : WebPartPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SPRibbon ribbon = SPRibbon.GetCurrent(this.Page);
                if (ribbon != null)
                {
                    ribbon.Visible = false;
                }
                SPUser currentUser = SPContext.Current.Web.CurrentUser;

                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite impersonatedSite = new SPSite(SPContext.Current.Web.Url))
                    {
                        using (SPWeb impersonatedWeb = impersonatedSite.OpenWeb())
                        {
                            impersonatedWeb.AllowUnsafeUpdates = true;

                            string OrdersListInternalName = SPUtility.GetLocalizedString("$Resources:OrdersListUrl", "Club0803_Config", 1033);
                            string listUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, OrdersListInternalName);
                            SPList OrdersList = impersonatedWeb.GetList(listUrl);

                            SPQuery query = new SPQuery();
                            query.Query = String.Format("<OrderBy><FieldRef Name=\"ID\" Ascending=\"FALSE\" /></OrderBy><Where><Eq><FieldRef Name=\"EmployeeName\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Eq></Where>", currentUser.ID);
                            query.ViewFields = "<FieldRef Name=\"ID\" /><FieldRef Name=\"Title\" /><FieldRef Name=\"DiscountedItem\" /><FieldRef Name=\"EmployeeName\" /><FieldRef Name=\"Rating\" /><FieldRef Name=\"Created\" /><FieldRef Name=\"DiscountedItemID\" /><FieldRef Name=\"Vendor\" />";
                            query.ViewFieldsOnly = true;
                            query.RowLimit = 1000;

                            repOrders.DataSource = OrdersList.GetItems(query).GetDataTable();
                            repOrders.DataBind();

                            impersonatedWeb.AllowUnsafeUpdates = false;
                        }
                    }
                });
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            GoBack();
        }

        private void GoBack()
        {
            if (Page.Request.QueryString["IsDlg"] != null)
            {
                HttpContext context = HttpContext.Current;
                context.Response.Write("<script type='text/javascript'>window.frameElement.cancelPopUp()</script>");
                context.Response.Flush();
                context.Response.End();
            }
            else if (!String.IsNullOrEmpty(Page.Request.QueryString["Source"]))
            {
                Response.Redirect(Page.Request.QueryString["Source"].ToString());
            }
            else
            {
                Response.Redirect(SPContext.Current.Web.Url);
            }
        }
    }
}

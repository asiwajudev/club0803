﻿using System;
using System.Web;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.WebPartPages;
using TAVIA.SharePoint.Utilities;

namespace TAVIA.SharePoint.Club0803.SitePagesModule
{
    public partial class NewCardRequest : WebPartPage
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            dtcDateRequired.MinDate = DateTime.Now.AddDays(-1);
            dtcDateRequired.MaxDate = DateTime.Now.AddYears(5);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SPRibbon ribbon = SPRibbon.GetCurrent(this.Page);
                if (ribbon != null)
                {
                    ribbon.Visible = false;
                }
                FillLocations();
                FillVendors();
            }
        }

        private void FillLocations()
        {
            SPList LocationsList = SPContext.Current.Web.Site.RootWeb.Lists["Locations"];
            ddlLocation.DataSource = LocationsList.Items.GetDataTable();
            ddlLocation.DataValueField = "ID";
            ddlLocation.DataTextField = "Title";
            ddlLocation.DataBind();
        }

        private void FillVendors()
        {
            string VendorsListInternalName = SPUtility.GetLocalizedString("$Resources:VendorsListUrl", "Club0803_Config", 1033);
            string listUrl = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, VendorsListInternalName);
            SPList VendorsList = SPContext.Current.Web.GetList(listUrl);
            ddlVendor.DataSource = VendorsList.Items.GetDataTable();
            ddlVendor.DataValueField = "ID";
            ddlVendor.DataTextField = "Title";
            ddlVendor.DataBind();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            GoBack();
        }

        private void GoBack()
        {
            if (Page.Request.QueryString["IsDlg"] != null)
            {
                HttpContext context = HttpContext.Current;
                context.Response.Write("<script type='text/javascript'>window.frameElement.cancelPopUp()</script>");
                context.Response.Flush();
                context.Response.End();
            }
            else if (!String.IsNullOrEmpty(Page.Request.QueryString["Source"]))
            {
                Response.Redirect(Page.Request.QueryString["Source"].ToString());
            }
            else
            {
                Response.Redirect(SPContext.Current.Web.Url);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;

            SPUser currentUser = SPContext.Current.Web.CurrentUser;

            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                using (SPSite impersonatedSite = new SPSite(SPContext.Current.Web.Url))
                {
                    using (SPWeb impersonatedWeb = impersonatedSite.OpenWeb())
                    {
                        impersonatedWeb.AllowUnsafeUpdates = true;

                        string CardRequestsListInternalName = SPUtility.GetLocalizedString("$Resources:CardRequestsListUrl", "Club0803_Config", 1033);
                        string listUrl = String.Format("{0}/lists/{1}", impersonatedWeb.Url, CardRequestsListInternalName);
                        SPList CardRequestsList = impersonatedWeb.GetList(listUrl);

                        SPListItem item = CardRequestsList.AddItem();

                        item["Vendor"] = new SPFieldLookupValue(int.Parse(ddlVendor.Text), ddlVendor.SelectedItem.Text);
                        item["JobLocation"] = new SPFieldLookupValue(int.Parse(ddlLocation.Text), ddlLocation.SelectedItem.Text);


                        if (!dtcDateRequired.IsDateEmpty)
                            item["DateRequired"] = dtcDateRequired.SelectedDate;

                        if (!String.IsNullOrEmpty(txtPhoneNumber.Text))
                            item["PhoneNumber"] = SPHandler.RemoveHTMLTags(txtPhoneNumber.Text);

                        if (!String.IsNullOrEmpty(txtComments.Text))
                            item["Comments"] = SPHandler.RemoveHTMLTags(txtComments.Text);

                        item["EmployeeName"] = new SPFieldUserValue(impersonatedWeb, currentUser.ID, currentUser.LoginName);

                        item.Update();

                        impersonatedWeb.AllowUnsafeUpdates = false;
                    }
                }
            });

            Response.Redirect("Info.aspx?msg=" + (int)Utility.Messages.Card_Requested_Successfully);
        }


    }
}

<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TAVIA.SharePoint.Club0803.SitePagesModule.Default" MasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <style>
        #sideNavBox {
            display: none;
        }

        #contentBox {
            margin-right: 20px;
            margin-left: 20px;
        }

        .h4, h4 {
            font-size: 1.5rem;
        }
        #s4-ribbonrow{
			display:none !important;
		}
		#s4-titlerow{
			display:none !important;
		}
		.ms-core-pageTitle{
			display:none !important;
		}

    </style>

    <SharePoint:UIVersionedContent ID="UIVersionedContent1" UIVersion="4" runat="server">
        <contenttemplate>
		<SharePoint:CssRegistration Name="forms.css" runat="server"/>
	</contenttemplate>
    </SharePoint:UIVersionedContent>

    <link href="/_layouts/15/club0803/styles/bootstrap.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-grid.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-reboot.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/style.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <section class="col-md-12 form-container pb-3">
        <h2 class="title-container text-center py-3">Welcome to Club0803</h2>
        <div class="form-bg">
            <div class="container">
                <div class="row p-3">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3 hvr-bounce-out">
                        <div class="card border-0 mtn-card">
                            <a class="text-center" href="ViewDiscountedItems.aspx">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images/view-discount-items.gif" alt="Card image top">
                                <div class="card-body text-center px-2">
                                    <span>View Discounted Items</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3 hvr-bounce-out">
                        <div class="card border-0 mtn-card">
                            <a class="text-center" href="MyOrders.aspx">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images/my-order.gif" alt="Card image top">
                                <div class="card-body text-center px-2">
                                    <span>My Orders</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3 hvr-bounce-out">
                        <div class="card border-0 mtn-card">
                            <a class="text-center" href="MyCart.aspx">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images/my-cart.gif" alt="Card image top">
                                <div class="card-body text-center px-2">
                                    <span>My Cart <asp:Label ID="lblMyCartCount" runat="server" ></asp:Label></span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3 hvr-bounce-out">
                        <div class="card border-0 mtn-card">
                            <a class="text-center" href="NewVendorSuggestion.aspx">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images/suggest-a-vendor.gif" alt="Card image top">
                                <div class="card-body text-center px-2">
                                    <span>Suggest a Vendor</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3 hvr-bounce-out">
                        <div class="card border-0 mtn-card">
                            <a class="text-center" href="NewCardRequest.aspx">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images/request-a-new-card.gif" alt="Card image top">
                                <div class="card-body text-center px-2">
                                    <span>Request A new Card</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <%--Admin--%>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3 hvr-bounce-out" id="divAdmin1" runat="server" visible="false">
                        <div class="card border-0 mtn-card">
                            <a class="text-center" href="../Lists/DiscountedItems/AllItems.aspx">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images/manage-discounted-items.gif" alt="Card image top">
                                <div class="card-body text-center px-2">
                                    <span>Manage Discounted Items</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3 hvr-bounce-out" id="divAdmin2" runat="server" visible="false">
                        <div class="card border-0 mtn-card">
                            <a class="text-center" href="../Lists/Orders/AllItems.aspx">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images/view-orders.gif" alt="Card image top">
                                <div class="card-body text-center px-2">
                                    <span>View Orders</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3" id="divAdmin3" runat="server" visible="false">
                        <div class="card border-0 mtn-card">
                            <span class="text-center">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images//manage-card.gif" alt="Card image top">
                               <div class="card-body text-center px-2">
<span>Manage Cards</span>
<div class="row px-3 pt-3 justify-content-center">
<a href="../Lists/Cards/AllItems.aspx" class="btn mtn-btn mr-3">Single</a>
<a href="../_layouts/15/MultipleCardUpload.aspx" class="btn mtn-btn">Bulk</a>
</div>
</div>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3 hvr-bounce-out" id="divAdmin4" runat="server" visible="false">
                        <div class="card border-0 mtn-card">
                            <a class="text-center" href="../Lists/CardRequests/AllItems.aspx">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images/view-card-request.gif" alt="Card image top">
                                <div class="card-body text-center px-2">
                                    <span>View Card Requests</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3 hvr-bounce-out" id="divAdmin5" runat="server" visible="false">
                        <div class="card border-0 mtn-card">
                            <a class="text-center" href="Reports.aspx">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images/reports.gif" alt="Card image top">
                                <div class="card-body text-center px-2">
                                    <span>Reports</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3 hvr-bounce-out" id="divAdmin6" runat="server" visible="false">
                        <div class="card border-0 mtn-card">
                            <a class="text-center" href="../Lists/Categories/AllItems.aspx">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images/manage-categories.gif" alt="Card image top">
                                <div class="card-body text-center px-2">
                                    <span>Manage Categories</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3 hvr-bounce-out" id="divAdmin7" runat="server" visible="false">
                        <div class="card border-0 mtn-card">
                            <a class="text-center" href="../Lists/CategoryTypes/AllItems.aspx">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images/manage-categories-type.gif" alt="Card image top">
                                <div class="card-body text-center px-2">
                                    <span>Manage Category Types</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3 hvr-bounce-out" id="divAdmin8" runat="server" visible="false">
                        <div class="card border-0 mtn-card">
                            <a class="text-center" href="../Lists/Vendors/AllItems.aspx">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images/manage-vendors.gif" alt="Card image top">
                                <div class="card-body text-center px-2">
                                    <span>Manage Vendors</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3 hvr-bounce-out" id="divAdmin9" runat="server" visible="false">
                        <div class="card border-0 mtn-card">
                            <a class="text-center" href="../Lists/VendorSuggestions/AllItems.aspx">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images/vendor-suggestion.gif" alt="Card image top">
                                <div class="card-body text-center px-2">
                                    <span>Vendor Suggestions</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Club0803
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" />

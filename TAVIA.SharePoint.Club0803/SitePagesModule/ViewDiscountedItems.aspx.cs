﻿using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.WebPartPages;

namespace TAVIA.SharePoint.Club0803.SitePagesModule
{
    public partial class ViewDiscountedItems : WebPartPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SPRibbon ribbon = SPRibbon.GetCurrent(this.Page);
                if (ribbon != null)
                {
                    ribbon.Visible = false;
                }
                FillVendors();
                FillCategories();

                FillDiscountedItems();
            }
        }

        void FillDiscountedItems()
        {

            SPUser currentUser = SPContext.Current.Web.CurrentUser;

            //SPSecurity.RunWithElevatedPrivileges(delegate ()
            //{
            //    using (SPSite impersonatedSite = new SPSite(SPContext.Current.Web.Url))
            //    {
            //        using (SPWeb impersonatedWeb = impersonatedSite.OpenWeb())
            //        {
            //            impersonatedWeb.AllowUnsafeUpdates = true;

            int vendorId = int.Parse(ddlVendor.Text);
            int categoryId = int.Parse(ddlCategory.Text);
            string productName = txtProductName.Text;

            string DiscountedItemsListInternalName = SPUtility.GetLocalizedString("$Resources:DiscountedItemsListUrl", "Club0803_Config", 1033);
            string listUrl = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, DiscountedItemsListInternalName);
            SPList DiscountedItemsList = SPContext.Current.Web.GetList(listUrl);

            StringBuilder strQuery = new StringBuilder("<OrderBy><FieldRef Name=\"Created\" Ascending=\"FALSE\" /></OrderBy>");
            strQuery.Append("<Where><And><And><And>");
            if (!String.IsNullOrEmpty(productName))
            {
                strQuery.Append("<And>");
            }
            strQuery.Append("<Eq><FieldRef Name=\"Status\" /><Value Type=\"Text\">Approved and Published</Value></Eq>");
            strQuery.Append("<Geq><FieldRef Name=\"ClosingDate\" /><Value Type=\"DateTime\"><Today /></Value></Geq></And>");
            if (vendorId > 0)
                strQuery.Append(String.Format("<Eq><FieldRef Name=\"VendorName\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Eq></And>", vendorId));
            else
                strQuery.Append(String.Format("<Neq><FieldRef Name=\"VendorName\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Neq></And>", vendorId));
            if (categoryId > 0)
                strQuery.Append(String.Format("<Eq><FieldRef Name=\"Category\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Eq></And>", categoryId));
            else
                strQuery.Append(String.Format("<Neq><FieldRef Name=\"Category\" LookupId=\"TRUE\" /><Value Type=\"Lookup\">{0}</Value></Neq></And>", categoryId));

            if (!String.IsNullOrEmpty(productName))
            {
                strQuery.Append(String.Format("<Contains><FieldRef Name=\"Title\" /><Value Type=\"Text\">{0}</Value></Contains>", productName));
                strQuery.Append("</And>");
            }

            strQuery.Append("</Where>");

            SPQuery query = new SPQuery();
            // query.Query = "<OrderBy><FieldRef Name=\"ClosingDate\" Ascending=\"FALSE\" /></OrderBy><Where><And><Eq><FieldRef Name=\"Status\" /><Value Type=\"Text\">Approved and Published</Value></Eq><Geq><FieldRef Name=\"ClosingDate\" /><Value Type=\"DateTime\"><Today /></Value></Geq></And></Where>";
            query.Query = strQuery.ToString();
            query.ViewFields = "<FieldRef Name=\"ID\" /><FieldRef Name=\"Title\" /><FieldRef Name=\"Category\" /><FieldRef Name=\"VendorName\" /><FieldRef Name=\"ClosingDate\" /><FieldRef Name=\"Status\" /><FieldRef Name=\"Attachments\" />";
            query.ViewFieldsOnly = true;
            query.RowLimit = 1000;

            SPListItemCollection colAllItems = DiscountedItemsList.GetItems(query);

            DataTable tblLatestItems = GetTableDefinition();
            DataTable tblAllItems = GetTableDefinition();

            foreach (SPListItem item in colAllItems)
            {
                DataRow row = tblAllItems.NewRow();

                row["ID"] = item.ID;
                row["Title"] = item.Title;
                row["ClosingDate"] = ((DateTime)item["ClosingDate"]).ToString("dd MMM yyyy");
                if (item.Attachments.Count > 0)
                {
                    row["ImgUrl"] = item.Attachments.UrlPrefix + item.Attachments[0];
                }
                else
                    row["ImgUrl"] = "";

                tblAllItems.Rows.Add(row);
            }

            int maxCount = 4;
            if (colAllItems.Count < 4)
                maxCount = colAllItems.Count;
            for (int i = 0; i < maxCount; i++)
            {
                DataRow row = tblLatestItems.NewRow();

                SPListItem item = colAllItems[i];
                row["ID"] = item.ID;
                row["Title"] = item.Title;
                row["ClosingDate"] = ((DateTime)item["ClosingDate"]).ToString("dd MMM yyyy");
                if (item.Attachments.Count > 0)
                {
                    row["ImgUrl"] = item.Attachments.UrlPrefix + item.Attachments[0];
                }
                else
                    row["ImgUrl"] = "";

                tblLatestItems.Rows.Add(row);
            }

            repLatestDiscountedItems.DataSource = tblLatestItems;
            repLatestDiscountedItems.DataBind();

            repDiscountedItems.DataSource = tblAllItems;
            repDiscountedItems.DataBind();

            //            impersonatedWeb.AllowUnsafeUpdates = false;
            //        }
            //    }
            //});
        }

        private DataTable GetTableDefinition()
        {
            DataTable tempTable = new DataTable();

            tempTable.Columns.Add("ID");
            tempTable.Columns.Add("Title");
            tempTable.Columns.Add("ClosingDate");
            tempTable.Columns.Add("ImgUrl");
            //tempTable.Columns.Add("");

            //query.ViewFields = "<FieldRef Name=\"ID\" />
            //<FieldRef Name=\"Title\" />
            //<FieldRef Name=\"Category\" />
            //<FieldRef Name=\"VendorName\" />
            //<FieldRef Name=\"ClosingDate\" />
            //<FieldRef Name=\"Status\" />
            //<FieldRef Name=\"Attachments\" />";

            return tempTable;
        }

        private void FillCategories()
        {
            string CategoriesListInternalName = SPUtility.GetLocalizedString("$Resources:CategoriesListUrl", "Club0803_Config", 1033);
            string listUrl = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, CategoriesListInternalName);
            SPList CategoriesList = SPContext.Current.Web.GetList(listUrl);
            ddlCategory.DataSource = CategoriesList.Items.GetDataTable();
            ddlCategory.DataValueField = "ID";
            ddlCategory.DataTextField = "Title";
            ddlCategory.DataBind();
            ddlCategory.Items.Insert(0, new ListItem("All Categories", "0"));
        }

        private void FillVendors()
        {
            string VendorsListInternalName = SPUtility.GetLocalizedString("$Resources:VendorsListUrl", "Club0803_Config", 1033);
            string listUrl = String.Format("{0}/lists/{1}", SPContext.Current.Web.Url, VendorsListInternalName);
            SPList VendorsList = SPContext.Current.Web.GetList(listUrl);
            ddlVendor.DataSource = VendorsList.Items.GetDataTable();
            ddlVendor.DataValueField = "ID";
            ddlVendor.DataTextField = "Title";
            ddlVendor.DataBind();
            ddlVendor.Items.Insert(0, new ListItem("All Vendors", "0"));
        }

        //protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    FillDiscountedItems();
        //}

        //protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    FillDiscountedItems();
        //}

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            GoBack();
        }

        private void GoBack()
        {
            if (Page.Request.QueryString["IsDlg"] != null)
            {
                HttpContext context = HttpContext.Current;
                context.Response.Write("<script type='text/javascript'>window.frameElement.cancelPopUp()</script>");
                context.Response.Flush();
                context.Response.End();
            }
            else if (!String.IsNullOrEmpty(Page.Request.QueryString["Source"]))
            {
                Response.Redirect(Page.Request.QueryString["Source"].ToString());
            }
            else
            {
                Response.Redirect(SPContext.Current.Web.Url);
            }
        }

        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            txtProductName.Text = String.Empty;
            ddlCategory.SelectedIndex = 0;
            ddlVendor.SelectedIndex = 0;
            FillDiscountedItems();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            FillDiscountedItems();
        }
    }
}

﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.WebPartPages;

namespace TAVIA.SharePoint.Club0803.SitePagesModule
{
    public partial class Reports : WebPartPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SPRibbon ribbon = SPRibbon.GetCurrent(this.Page);
                if (ribbon != null)
                {
                    ribbon.Visible = false;
                }

                string AdminsGroupName = SPUtility.GetLocalizedString("$Resources:AdminsGroupName", "Club0803_Config", 1033);
                string SuperAdminsGroupName = SPUtility.GetLocalizedString("$Resources:SuperAdminsGroupName", "Club0803_Config", 1033);

                if (SPContext.Current.Web.SiteGroups[AdminsGroupName].ContainsCurrentUser ||
                    SPContext.Current.Web.SiteGroups[SuperAdminsGroupName].ContainsCurrentUser)
                {
                    //Allowed
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }

            }
        }
    }
}

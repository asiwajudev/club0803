﻿using System;
using System.Web;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.WebPartPages;

namespace TAVIA.SharePoint.Club0803.SitePagesModule
{
    public partial class Info : WebPartPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SPRibbon ribbon = SPRibbon.GetCurrent(this.Page);
                if (ribbon != null)
                {
                    ribbon.Visible = false;
                }

                if (!String.IsNullOrEmpty(Page.Request.QueryString["msg"]))
                {
                    int msg;
                    if (int.TryParse(Page.Request.QueryString["msg"], out msg))
                    {
                        if (typeof(Utility.Messages).IsEnumDefined(msg))
                        {
                            Utility.Messages message = (Utility.Messages)msg;
                            if (message == Utility.Messages.Disocunted_Item_Does_Not_Exist_Or_Expired)
                            {
                                lblMessage.Text = "Item does not exist or may have expired.";
                            }
                            else if (message == Utility.Messages.Order_Submitted_Successfully)
                            {
                                lblMessage.Text = "Your order has been submitted successfully.";
                            }
                            else if (message == Utility.Messages.Vendor_Suggestion_Submitted_Successfully)
                            {
                                lblMessage.Text = "Your vendor suggestion has been submitted successfully.";
                            }
                            else if (message == Utility.Messages.Card_Requested_Successfully)
                            {
                                lblMessage.Text = "Your card request has been submitted successfully.";
                            }
                            else if (message == Utility.Messages.Rating_Submitted_Successfully)
                            {
                                lblMessage.Text = "Your rating has been submitted successfully.";
                            }
                        }
                    }
                }

            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            GoBack();
        }

        private void GoBack()
        {
            if (Page.Request.QueryString["IsDlg"] != null)
            {
                HttpContext context = HttpContext.Current;
                context.Response.Write("<script type='text/javascript'>window.frameElement.cancelPopUp()</script>");
                context.Response.Flush();
                context.Response.End();
            }
            else if (!String.IsNullOrEmpty(Page.Request.QueryString["Source"]))
            {
                Response.Redirect(Page.Request.QueryString["Source"].ToString());
            }
            else
            {
                Response.Redirect(SPContext.Current.Web.Url);
            }
        }
    }
}

﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="TAVIA.SharePoint.Club0803.SitePagesModule.Reports" MasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <style>
        #sideNavBox {
            display: none;
        }

        #contentBox {
            margin-right: 20px;
            margin-left: 20px;
        }

        .h4, h4 {
            font-size: 1.5rem;
        }
        #s4-ribbonrow{
			display:none !important;
		}
		#s4-titlerow{
			display:none !important;
		}
		.ms-core-pageTitle{
			display:none !important;
		}

    </style>

    <SharePoint:UIVersionedContent ID="UIVersionedContent1" UIVersion="4" runat="server">
        <contenttemplate>
		<SharePoint:CssRegistration Name="forms.css" runat="server"/>
	</contenttemplate>
    </SharePoint:UIVersionedContent>

    <link href="/_layouts/15/club0803/styles/bootstrap.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-grid.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-reboot.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/style.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <section class="col-md-12 form-container pb-3">
        <h2 class="title-container text-center py-3">Club0803 Reports</h2>
        <div class="form-bg">
            <div class="container">
                <div class="row p-3">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3 hvr-bounce-out" >
                        <div class="card border-0 mtn-card">
                            <a class="text-center" href="../Lists/DiscountedItems/ProductsReport.aspx">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images/reports.gif" alt="Card image top">
                                <div class="card-body text-center px-2">
                                    <span>Products & Services Report</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3 hvr-bounce-out" >
                        <div class="card border-0 mtn-card">
                            <a class="text-center" href="../Lists/DiscountedItems/BenefitsReport.aspx">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images/reports.gif" alt="Card image top">
                                <div class="card-body text-center px-2">
                                    <span>Benefits Report</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3 hvr-bounce-out" >
                        <div class="card border-0 mtn-card">
                            <a class="text-center" href="../Lists/Vendors/Report.aspx">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images/reports.gif" alt="Card image top">
                                <div class="card-body text-center px-2">
                                    <span>Vendors Report</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 py-3 hvr-bounce-out" >
                        <div class="card border-0 mtn-card">
                            <a class="text-center" href="../Lists/Orders/Report.aspx">
                                <img class="card-img-top p-4" src="/_layouts/15/club0803/images/reports.gif" alt="Card image top">
                                <div class="card-body text-center px-2">
                                    <span>Orders Report</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Club0803 - Report
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" />

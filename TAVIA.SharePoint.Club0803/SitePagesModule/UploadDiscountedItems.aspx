﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadDiscountedItems.aspx.cs" Inherits="TAVIA.SharePoint.Club0803.SitePagesModule.UploadDiscountedItems" MasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <style>
        #sideNavBox {
            display: none;
        }

        #contentBox {
            margin-right: 20px;
            margin-left: 20px;
        }

        .h4, h4 {
            font-size: 1.5rem;
        }
        #s4-ribbonrow{
			display:none !important;
		}
		#s4-titlerow{
			display:none !important;
		}
		.ms-core-pageTitle{
			display:none !important;
		}

    </style>

    <SharePoint:UIVersionedContent ID="UIVersionedContent1" UIVersion="4" runat="server">
        <contenttemplate>
		<SharePoint:CssRegistration Name="forms.css" runat="server"/>
	</contenttemplate>
    </SharePoint:UIVersionedContent>

    <link href="/_layouts/15/club0803/styles/bootstrap.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-grid.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/bootstrap-reboot.min.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/styles/style.css" rel="stylesheet" />
    <link href="/_layouts/15/club0803/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <section class="col-md-12 form-container pb-3">
        <h2 class="title-container text-center py-3">Multiple Upload Discounted Items</h2>
        <div class="form-bg">
            <!-- section2 -->
            <div class="row p-3">
                <div class="col-md-8">
                    <h5 class="text-blue">Admin Details</h5>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Full Name</label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblEmployeeName" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Email</label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblEmployeeEmailAddress" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <hr class="mtn-divider m-0 px-3" />
            </div>
            <!-- section3 -->
            <div class="row p-3">
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Category<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:DropDownList ID="ddlCategory" class="form-control mtn-input" runat="server" AutoPostBack="true"
                        OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                            </asp:DropDownList>
                            <SharePoint:InputFormRequiredFieldValidator ID="rfvCategory"
                                class="ms-error" ControlToValidate="ddlCategory" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Type<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                        <asp:DropDownList ID="ddlType" class="form-control mtn-input" runat="server">
                        </asp:DropDownList>
                        <SharePoint:InputFormRequiredFieldValidator ID="rfvType"
                            class="ms-error" ControlToValidate="ddlType" Text="Error" runat="server"
                            ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                            Display="Dynamic">
                        </SharePoint:InputFormRequiredFieldValidator>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <hr class="mtn-divider m-0 px-3" />
            </div>
            <!-- section3 -->
            <div class="row p-3">
                <div class="col-md-12">
                    <h5 class="text-blue">Vendor Details</h5>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Vendor Name<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:DropDownList ID="ddlVendor" class="form-control mtn-input" runat="server" AutoPostBack="true"
                        OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged">
                            </asp:DropDownList>
                            <SharePoint:InputFormRequiredFieldValidator ID="rfvVendor"
                                class="ms-error" ControlToValidate="ddlVendor" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Contact Person<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblVendorContactPerson" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Phone Number<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblVendorPhoneNumber" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Contact Email<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblVendorContactEmail" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Contact Address<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                            <asp:Label ID="lblVendorContactAddress" Text="&nbsp;" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-md-5 col-sm-5 col-12">
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <hr class="mtn-divider m-0 px-3" />
            </div>

            <div class="row p-3">
                <div class="col-md-12">
                    <h5 class="text-blue">Discounted Item Details</h5>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="row py-2" id="divUpload1" runat="server">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Template Download</label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                              <a href="/_Layouts/15/club0803/MultipleDiscountedItems.xlsx">Multiple Discounted Items Template</a><br />
                              <span style="color: red">Please note to fill all mandatory fields which are in bold red in the excel template</span>
                        </div>
                    </div>
                    <div class="row py-2" id="divUpload2" runat="server">
                        <div class="col-md-5 col-sm-5 col-12">
                            <label>Template Upload<span class="text-red">*</span></label>
                        </div>
                        <div class="col-md-7 col-sm-7 col-12">
                                <asp:FileUpload ID="fuMultipleCandidates" runat="server" EnableViewState="true" class="form-control" />
                                <span style="color: red">File Format: xls, xlsx</span>
                                <SharePoint:InputFormRegularExpressionValidator ID="rgxvMultipleCandidates" runat="server"
                                    ErrorMessage="Only files with these extensions are allowed .xls .xlsx"
                                    ValidationExpression=".*(\.xls|\.XLS|\.xlsx|\.XLSX)$"
                                    ControlToValidate="fuMultipleCandidates"></SharePoint:InputFormRegularExpressionValidator>
                                <SharePoint:InputFormRequiredFieldValidator ID="rfvMultipleCandidates"
                                    class="ms-error" ControlToValidate="fuMultipleCandidates" Text="Error" runat="server"
                                    ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                    Display="Dynamic" BreakBefore="false">
                                </SharePoint:InputFormRequiredFieldValidator>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row px-4">
                    <div class="col-md-12">
                        <div class="row pb-4">
                            <div class="table-responsive text-nowrap table-hover pt-4">
                                <asp:Repeater ID="repDiscountedItems" runat="server">
                                    <HeaderTemplate>
                                        <table class="table mb-0 table-bordered t-table que-table" id="example">
                                            <thead>
                                                <tr>
                                                    <th>Product Name</th>
                                                    <th>Description of the Product</th>
                                                    <th>Vendor/Product Website URL</th>
                                                    <th>Closing Date</th>
                                                    <th>Payment Options</th>
                                                    <th>Interest Rate</th>
                                                    <th>Discount Rate</th>
                                                    <th>Any other important information</th>
                                                    <th>Product Price</th>
                                                    <th>Upload Image</th>
                                                </tr>
                                            </thead>
                                            <tbody class="">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td class=""><asp:Label ID="lblProductName" runat="server" Text='<%# Eval("ProductName") %>'></asp:Label></td>
                                            <td class=""><asp:Label ID="lblProductDescription" runat="server" Text='<%# Eval("ProductDescription") %>'></asp:Label></td>
                                            <td class=""><asp:Label ID="lblVendorProductWebsiteURL" runat="server" Text='<%# Eval("VendorProductWebsiteURL") %>'></asp:Label></td>
                                            <td class=""><asp:Label ID="lblClosingDate" runat="server" Text='<%# Eval("ClosingDate") %>'></asp:Label></td>
                                            <td class=""><asp:Label ID="lblPaymentOptions" runat="server" Text='<%# Eval("PaymentOptions") %>'></asp:Label></td>
                                            <td class=""><asp:Label ID="lblInterestRate" runat="server" Text='<%# Eval("InterestRate") %>'></asp:Label></td>
                                            <td class=""><asp:Label ID="lblDiscountRate" runat="server" Text='<%# Eval("DiscountRate") %>'></asp:Label></td>
                                            <td class=""><asp:Label ID="lblOtherImportantInformation" runat="server" Text='<%# Eval("OtherImportantInformation") %>'></asp:Label></td>
                                            <td class=""><asp:Label ID="lblProductPrice" runat="server" Text='<%# Eval("ProductPrice") %>'></asp:Label></td>
                                            <td class="">
                                                <asp:FileUpload ID="fuAttachDocument" runat="server" EnableViewState="true" CssClass="form-control mtn-input mtn-textbox" Width="400px"/>
                                                <span style="color: red">&nbsp;jpg; jpeg; png;</span>
                                                <SharePoint:InputFormRegularExpressionValidator ID="rgxvAttachDocument" runat="server"
                                                    ErrorMessage="Only files with these extensions are allowed .jpg .jpeg .png"
                                                    ValidationExpression=".*(\.jpg|\.JPG|\.jpeg|\.JPEG|\.png|\.PNG)$"
                                                    ControlToValidate="fuAttachDocument"></SharePoint:InputFormRegularExpressionValidator>
                                                <SharePoint:InputFormRequiredFieldValidator ID="rfvAttachDocument" class="ms-error"
                                                    ControlToValidate="fuAttachDocument" Text="Error" runat="server" ErrorMessage="You must specify a value for this required field."
                                                    EnableClientScript="true" Display="Dynamic" BreakBefore="true">
                                                </SharePoint:InputFormRequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                    </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="col-md-12">
                <hr class="mtn-divider m-0 px-3" />
            </div>
            <!-- button section -->
            <div class="col-md-12 py-3">
                <div class="row btn-div text-center">
                    <asp:Button ID="btnSaveItems" class="btn mtn-btn mr-3" runat="server" Text="Submit" OnClick="btnSaveItems_Click" Visible="false" />
                    <asp:Button ID="btnSubmit" class="btn mtn-btn mr-3" runat="server" Text="Upload" OnClick="btnSubmit_Click" />
                    <asp:Button ID="btnCancel" class="btn mtn-btn mr-3" CausesValidation="false" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                </div>
            </div>
        </div>
    </section>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Club0803 - Multiple Upload Discounted Items
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" />
